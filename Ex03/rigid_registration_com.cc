// ===============================
//
// 3D IMAGE ANALYSIS
//
// Exercise 03. Rigid registration 
// Vladislav Tananaev
// tananaev@cs.uni-freiburg.de
// v.d.tananaev@gmail.com
//
// ===============================
#include <iostream>
#include <fstream>
#include <sstream>
#include <iomanip>
#include <blitz/array.h>
#include <string>
#include <math.h>
#include <limits>

// Creates orthogonal maximum intensity projections of the given 3D array
blitz::Array<unsigned char, 2> orthoMips (const blitz::Array<unsigned char, 3> &arr);

blitz::TinyMatrix<float, 4, 4> myproduct (blitz::TinyMatrix<float, 4, 4> a, blitz::TinyMatrix<float, 4, 4> b);
blitz::TinyVector<float, 4> myproduct (blitz::TinyMatrix<float, 4, 4> m, blitz::TinyVector<float, 4> v);

unsigned char interpolNN (const blitz::Array<unsigned char, 3> &arr, blitz::TinyVector<float, 4> pos);

float ssdOfFixedImAndTransformedMovingIm (const blitz::Array<unsigned char, 3> &movingIm, 
                                          const blitz::TinyMatrix<float, 4, 4> &invMat,
                                          const blitz::Array<unsigned char, 3> &fixedIm,
                                          int step);


void transformArray (const blitz::Array<unsigned char, 3> &srcArr, 
		     const blitz::TinyMatrix<float, 4, 4> &invMat,
		     blitz::Array<unsigned char, 3>  &trgArr);

blitz::TinyMatrix<float, 4, 4> createInverseRigidTransMatrix (const blitz::TinyVector<size_t, 3> &srcArrShape,
				const blitz::TinyVector<size_t, 3> &trgArrShape,
				const blitz::TinyVector<float, 3> &src_element_size_um,
				const blitz::TinyVector<float, 3> &trg_element_size_um,
                                blitz::TinyVector<float, 6> params);

// Computer center of gravity and returns a vecor in homogeneous coordinates
blitz::TinyVector<float, 4> computeCOG( const blitz::Array<unsigned char, 3> &srcArr );

void savePPMImage( const blitz::Array<blitz::TinyVector<unsigned char,3>,2>& image,
                   const std::string& fileName);

int main (int argc, char** argv)
{ 
  // Define dataset properties
  const char* filename1 = "leaf_t5_150x521x396_8bit.raw";
  const char* filename2 = "leaf_t6_150x521x396_8bit.raw";
  int nLevels = 150;
  int nRows = 521;
  int nCols = 396;
  
  blitz::Array<unsigned char, 3> staticIm (nLevels, nRows, nCols);
  blitz::Array<unsigned char, 3> movingIm (nLevels, nRows, nCols);

  // Load first file
  std::ifstream file1 (filename1, std::ifstream::in | std::ifstream::binary );
  file1.read (reinterpret_cast<char*>(staticIm.data()), staticIm.size()*sizeof(char));
  file1.close();
  
  // Load second file
  std::ifstream file2 (filename2, std::ifstream::in | std::ifstream::binary );
  file2.read (reinterpret_cast<char*>(movingIm.data()), movingIm.size()*sizeof(char));
  file2.close();

  // Big images for orthoMips
  blitz::Array<unsigned char, 2> firstOrtho(nLevels+nRows+1, nCols+ nLevels+1);
  blitz::Array<unsigned char, 2> secondOrtho(nLevels+nRows+1, nCols+ nLevels+1);

  blitz::Array<unsigned char, 3> trgArr(nLevels, nRows, nCols);

  blitz::TinyVector<float, 3> src_element_size_um(2.0, 1.46484, 1.46484);
  blitz::TinyVector<float, 3> trg_element_size_um(2.0, 1.46484, 1.46484);

  // Pre-Align
  blitz::TinyVector<float, 4> cogFirst, cogSecond, translation;
  cogFirst = computeCOG(staticIm);
  std::cout << cogFirst << std::endl;
  cogSecond = computeCOG(movingIm);
  std::cout << cogSecond << std::endl;
  translation = cogSecond - cogFirst;
  std::cout << translation << std::endl;

	

  blitz::TinyVector<float, 6> params( translation(0), translation(1), translation(2), 0, 0, 0 );
  blitz::TinyMatrix<float, 4, 4> invMat = createInverseRigidTransMatrix(movingIm.shape(), 
                                                                              movingIm.shape(), 
                                                                              src_element_size_um, 
                                                                              trg_element_size_um, 
                                                                              params);

  transformArray(movingIm, invMat, trgArr);
  movingIm = trgArr;

  firstOrtho = orthoMips(staticIm); 
  secondOrtho = orthoMips(movingIm); 

  blitz::Array<blitz::TinyVector<unsigned char, 3>, 2> resultingImage(nLevels+nRows+1, nCols+nLevels+1);
  resultingImage[0] = firstOrtho;
  resultingImage[1] = secondOrtho;
  resultingImage[2] = 0;
      
  // Write first output
  const std::string outf= "iter_000.ppm";
  savePPMImage( resultingImage, outf );

  blitz::Array<float, 2> neighborDirections(12, 6);
  neighborDirections =  1,  0,  0,  0,  0,  0,
                       -1,  0,  0,  0,  0,  0,
                        0,  1,  0,  0,  0,  0,
                        0, -1,  0,  0,  0,  0,
                        0,  0,  1,  0,  0,  0,
                        0,  0, -1,  0,  0,  0,
                        0,  0,  0,  1,  0,  0,
                        0,  0,  0, -1,  0,  0,
                        0,  0,  0,  0,  1,  0,
                        0,  0,  0,  0, -1,  0,
                        0,  0,  0,  0,  0,  1,
                        0,  0,  0,  0,  0, -1;

  blitz::TinyVector<float, 6> currentParams;
  blitz::TinyVector<float, 6> lastParams;
  blitz::TinyVector<float, 6> bestParams;
  currentParams = 0;
  lastParams = 0;
  bestParams = 0;
  
  float stepsize = 16;
  float ssd = std::numeric_limits<float>::max();
  float bestSSD = ssd;
  bool success=false;
  int iter(1);
  trgArr = movingIm;

  while (stepsize >= 0.125){


    for (int row = 0; row < 12; row++) // for each neighbour
    {

      currentParams = lastParams + neighborDirections(row, blitz::Range::all()) * stepsize;

      blitz::TinyMatrix<float, 4, 4> invMat = createInverseRigidTransMatrix(movingIm.shape(), 
                                                                            movingIm.shape(), 
                                                                            src_element_size_um, 
                                                                            trg_element_size_um, 
                                                                            currentParams);

       ssd = ssdOfFixedImAndTransformedMovingIm(movingIm, invMat, staticIm, 4);
        
       if ( ssd < bestSSD ){
         bestSSD = ssd; 
         bestParams = currentParams;
         success=true; 
       }

    }
    
    lastParams = bestParams;
    
    // We failed to find best neighbour here
    if ( !success ){

      stepsize /= 2;

    } else {

        blitz::TinyMatrix<float, 4, 4> invMat = createInverseRigidTransMatrix(movingIm.shape(), 
                                                                              movingIm.shape(), 
                                                                              src_element_size_um, 
                                                                              trg_element_size_um, 
                                                                              bestParams);

        // std::cout << "Iteration " << iter <<  " with transformation: " << bestParams << std::endl;

        transformArray(movingIm, invMat, trgArr);
        firstOrtho = orthoMips(staticIm); 
        secondOrtho = orthoMips(trgArr); 

        blitz::Array<blitz::TinyVector<unsigned char, 3>, 2> resultingImage(nLevels+nRows+1, nCols+nLevels+1);
        resultingImage[0] = firstOrtho;
        resultingImage[1] = secondOrtho;
        resultingImage[2] = 0;
      
        // Write output
        std::ostringstream out_file;
        out_file << "iter_" << std::setw(3) << std::setfill('0') << iter << ".ppm";
        savePPMImage( resultingImage, out_file.str() );
        ++iter;
        success = false;

    }

  }

  std::cout<<"Resulting transformation vector: " << bestParams << std::endl;
  std::cout<<"Final SSD: " << bestSSD << std::endl;
  std::cout<<"Steps: " << iter << std::endl;

  return 0;

}


blitz::TinyMatrix<float, 4, 4> myproduct (blitz::TinyMatrix<float, 4, 4> a, blitz::TinyMatrix<float, 4, 4> b)
{
  blitz::TinyMatrix<float, 4, 4> result;
  float sum (0); 
  for (int lev = 0; lev < a.extent (0); ++lev) {
    for (int row = 0; row < a.extent (0); ++row) {
      for (int col = 0; col < a.extent (1); ++col) {
	sum = sum + a (lev, col) * b (col, row);
      }
      result (lev,row) = sum;
      sum = 0;
    }
  }
  return result;
}


blitz::TinyVector<float, 4> myproduct (blitz::TinyMatrix<float, 4, 4> m, blitz::TinyVector<float, 4> v)
{
  blitz::TinyVector<float, 4> result;

  float sum (0);

  for (int row = 0; row < m.extent (0); ++row) {
    for (int col = 0; col < m.extent (1); ++col) {
      sum = sum + m (row, col) * v (col);
    }
    result (row) = sum;
    sum = 0;
  }

  return result;
}

unsigned char interpolNN (const blitz::Array<unsigned char, 3> &arr, blitz::TinyVector<float, 4> pos)
{
  blitz::TinyVector<float, 4> new_pos = blitz::floor( pos + 0.5f ); 
  if ( blitz::all( new_pos >= 0 ) && blitz::all( new_pos < arr.shape() ) )
    return arr ( new_pos );
  else
    return 0;
}

float ssdOfFixedImAndTransformedMovingIm (const blitz::Array<unsigned char, 3> &movingIm, 
                                          const blitz::TinyMatrix<float, 4, 4> &invMat,
                                          const blitz::Array<unsigned char, 3> &fixedIm,
                                          int step)
{

  float ssd(0);
  blitz::TinyVector<float, 4> trgPos, srcPos;
  blitz::Array<unsigned char, 3> trgArr(fixedIm.extent(0), fixedIm.extent(1), fixedIm.extent(2) );

  for (int lev = 0; lev < trgArr.extent(0); lev+=step){ 
    for (int row = 0; row < trgArr.extent(1); row+=step){ 
      for (int col = 0; col < trgArr.extent(2); col+=step){ 

	trgPos = lev, row, col, 1;
	srcPos = myproduct (invMat, trgPos);
	trgArr (lev, row, col) = interpolNN (movingIm, srcPos);

        ssd += blitz::pow2( trgArr(lev, row, col) - fixedIm(lev, row, col) );
      }
    }
  }

  return ssd;
}

void transformArray (const blitz::Array<unsigned char, 3> &srcArr, 
		     const blitz::TinyMatrix<float, 4, 4> &invMat,
		     blitz::Array<unsigned char, 3>  &trgArr)
{

  blitz::TinyVector<float, 4> trgPos, srcPos;
  for (int lev = 0; lev < trgArr.extent(0); ++lev){ 
    for (int row = 0; row < trgArr.extent(1); ++row){ 
      for (int col = 0; col < trgArr.extent(2); ++col){ 

	trgPos = lev, row, col, 1;
	srcPos = myproduct (invMat, trgPos);
	trgArr (lev, row, col) = interpolNN (srcArr, srcPos);

      }
    }
  }

}

blitz::TinyMatrix<float, 4, 4> createInverseRigidTransMatrix (const blitz::TinyVector<size_t, 3> &srcArrShape,
				const blitz::TinyVector<size_t, 3> &trgArrShape,
				const blitz::TinyVector<float, 3> &src_element_size_um,
				const blitz::TinyVector<float, 3> &trg_element_size_um,
				blitz::TinyVector<float, 6> params )
{

  float shiftLev = params(0);
  float shiftRow = params(1);
  float shiftCol = params(2);
  float angleAroundLev = params(3);
  float angleAroundRow = params(4);
  float angleAroundCol = params(5);

  blitz::TinyMatrix<float, 4, 4> result;
  
  // Definition of the seven matrixes
  blitz::TinyMatrix<float, 4, 4> shiftTrgCenterToOrigin, scaleToMicrometer, rotateAroundLev, rotateAroundRow, rotateAroundCol;
  blitz::TinyMatrix<float, 4, 4> scaleToVoxel, shiftOriginToScrCenter;
  
  shiftTrgCenterToOrigin = 1, 0, 0, -float(trgArrShape(0))/2,
			   0, 1, 0, -float(trgArrShape(1))/2,
			   0, 0, 1, -float(trgArrShape(2))/2,
			   0, 0, 0, 1;

  float sx = src_element_size_um(0);
  float sy = src_element_size_um(1);
  float sz = src_element_size_um(2);
  scaleToMicrometer = sx,  0,  0, 0,
		       0, sy,  0, 0,
		       0,  0, sz, 0,
		       0,  0,  0, 1;

  float a = - (angleAroundCol*M_PI) / 180;
  rotateAroundCol = cos(a), -sin(a), 0, 0,
		    sin(a),  cos(a), 0, 0,
		         0,       0, 1, 0,
		         0,       0, 0, 1;

  a = - (angleAroundRow*M_PI) / 180;
  rotateAroundRow =   cos(a),   0,  sin(a), 0,
		           0,   1,       0, 0,
		     -sin(a),   0,  cos(a), 0,
		           0,   0,       0, 1;

  a = - (angleAroundLev*M_PI) / 180;
  rotateAroundLev = 1,      0,       0, 0, 
                    0, cos(a), -sin(a), 0,
		    0, sin(a),  cos(a), 0,
		    0,      0,       0, 1;

  sx = 1.0/trg_element_size_um(0);
  sy = 1.0/trg_element_size_um(1);
  sz = 1.0/trg_element_size_um(2);
  scaleToVoxel = sx,  0,  0, 0,
		  0, sy,  0, 0,
		  0,  0, sz, 0,
		  0,  0,  0, 1;

  shiftOriginToScrCenter = 1, 0, 0, float(srcArrShape(0))/2 + shiftLev,
			   0, 1, 0, float(srcArrShape(1))/2 + shiftRow,
			   0, 0, 1, float(srcArrShape(2))/2 + shiftCol,
			   0, 0, 0, 1;

  result = myproduct(scaleToMicrometer, shiftTrgCenterToOrigin);
  result = myproduct(rotateAroundCol, result);
  result = myproduct(rotateAroundRow, result);
  result = myproduct(rotateAroundLev, result);
  result = myproduct(scaleToVoxel, result);
  result = myproduct(shiftOriginToScrCenter, result);
  
  return result;
}

void savePPMImage( const blitz::Array<blitz::TinyVector<unsigned char,3>,2>& image,
                   const std::string& fileName)
{

  std::ofstream outFile ( fileName.c_str(), std::ofstream::binary);
  
  outFile << "P6\n" << image.extent(1) << " " << image.extent(0) << " 255\n";

  outFile.write( reinterpret_cast<const char*>(image.dataFirst()), 
                 image.size() * sizeof( blitz::TinyVector<unsigned char,3>));

}

blitz::Array<unsigned char, 2> orthoMips (const blitz::Array<unsigned char, 3> &arr)
{
  int nlevs = arr.extent(0);
  int nrows = arr.extent(1);
  int ncols = arr.extent(2);
  
  // Create a big image to which we'll write results
  int rows = nlevs + nrows + 1;
  int cols = ncols + nlevs + 1;
  blitz::Array<unsigned char, 2> result (rows, cols);
  result = 0;

  for (int lev=0; lev<nlevs; ++lev)
  for (int row=0; row<nrows; ++row)
  for (int col=0; col<ncols; ++col)
  {

    if ( result(lev, col) < arr(lev, row, col) )
     result(lev,col) = arr(lev, row, col);

    if ( result(row+nlevs+1, col) < arr(lev, row, col) )
     result(row+nlevs+1,col) = arr(lev, row, col);

    if ( result(row+nlevs+1, lev+ncols+1) < arr(lev, row, col) )
     result(row+nlevs+1,lev+ncols+1) = arr(lev, row, col);
    

  }

  return result;
}

blitz::TinyVector<float, 4> computeCOG( const blitz::Array<unsigned char, 3> &srcArr )
{
  blitz::TinyVector<float, 4> cog;

  int nlevs = srcArr.extent(0);  
  int nrows = srcArr.extent(1);  
  int ncols = srcArr.extent(2);  

  float lev_sum(0), row_sum(0), col_sum(0);
  float normalizer(0);

  for (int lev=0; lev<nlevs; ++lev)
  for (int row=0; row<nrows; ++row)
  for (int col=0; col<ncols; ++col){

      float value = srcArr(lev,row,col);
      lev_sum += lev*value;
      row_sum += row*value;
      col_sum += col*value;
      normalizer+= value;


  } 
  cog(0) = lev_sum/normalizer;
  cog(1) = row_sum/normalizer;
  cog(2) = col_sum/normalizer;
  cog(3) = 1;
  return cog;

}


