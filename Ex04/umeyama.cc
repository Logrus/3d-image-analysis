// ===============================
//
// 3D IMAGE ANALYSIS
//
// Exercise 04. Landmark based registration with Umeyama Method
// Vladislav Tananaev
// tananaev@cs.uni-freiburg.de
// v.d.tananaev@gmail.com
//
// ===============================

#include "BlitzHDF5Helper.hh"
#include "my_blitz.hh"
#include <iostream>
#include <fstream>
#include <sstream>
#include <iomanip>
#include <blitz/array.h>
#include <string>
#include <math.h>
#include <limits>
#include <gsl/gsl_matrix.h>
#include <gsl/gsl_linalg.h>

using namespace std;

#define MAXFLOAT (std::numeric_limits<float>::max())


// Overloaded interpolNN
float interpolNN (const blitz::Array<float, 3> &arr, blitz::TinyVector<float, 4> pos);

// Overload of transformArray
void transformArray (const blitz::Array<float, 3> &srcArr, 
		     const blitz::TinyMatrix<float, 4, 4> &invMat,
		     blitz::Array<float, 3>  &trgArr);

// Mark Landmark
// Highlights the landmarks with a cube
void markLandmark ( blitz::Array<float, 3> &Image,
                    const blitz::Array<blitz::TinyVector<double, 4>, 1> &landmarks,
                    blitz::TinyVector<float, 3> element_size_um );

// Takes two images as input and returns rgb image
blitz::Array< blitz::TinyVector<float, 3>, 3> overlay (const blitz::Array<float, 3> &fixedImage,
                                                       const blitz::Array<float, 3> &movingImage);

// Wraps gsl sv_decom function
void mySVD( blitz::TinyMatrix<float, 3, 3> &A,
            blitz::TinyVector<float, 3> &S,
            blitz::TinyMatrix<float, 3, 3> &V);


blitz::TinyMatrix<float, 4, 4> umeyama( const blitz::Array<blitz::TinyVector<double, 4>, 1> &points,
                                        const blitz::Array<blitz::TinyVector<double, 4>, 1> &transformedPoints);

// Computes mean of the point set
// return value in homogeneous coordinates
blitz::TinyVector<float, 4> computeCOMOfLandmarks( const blitz::Array<blitz::TinyVector<double, 4>, 1> &landmarks);

// Constructs a covariance matrix
blitz::TinyMatrix<double, 3, 3> makeCovMatrix( const blitz::Array<blitz::TinyVector<double,4>, 1> X,
                                               const blitz::Array<blitz::TinyVector<double,4>, 1> Y);


float absoluteDistance ( const blitz::Array<float, 3>  &fixedIm,
                        const blitz::Array<float, 3> &movingIm)
{

  float distance(0);

  for (int lev = 0; lev < fixedIm.extent(0); lev++){ 
    for (int row = 0; row < fixedIm.extent(1); row++){ 
      for (int col = 0; col < fixedIm.extent(2); col++){ 
        distance += fabs( movingIm(lev, row, col) - fixedIm(lev, row, col) );
      }
    }
  }

  return distance;
}

int main()
{

  // Filenames
  std::string inFileName0="zebrafish0.h5";
  std::string inFileName1="zebrafish1.h5";

  blitz::Array<float, 3> zebrafish0, zebrafish1;
  // Load data first dataset
  readHDF5toBlitz (inFileName0, "channel0", zebrafish0);
  blitz::TinyVector<float, 3> element_size_um;
  readBlitzTinyVectorFromHDF5Attribute (element_size_um, "element_size_um", "/channel0", inFileName0);

  // Load second dataset
  readHDF5toBlitz (inFileName1, "channel0", zebrafish1);
  
  // Load landmarks from the hdf5
  blitz::Array<blitz::TinyVector<double,3>, 1> landmarks0, landmarks1;
  readHDF5toBlitz (inFileName0, "landmarks_um", landmarks0);
  readHDF5toBlitz (inFileName1, "landmarks_um", landmarks1);

  // Make vectors homogeneous
  int n_land = landmarks0.extent(0);
  blitz::Array<blitz::TinyVector<double,4>, 1> landmarks0_new(n_land), landmarks1_new(n_land);
  for (int i=0; i<landmarks0.extent(0); ++i){
  
    landmarks0_new(i) = landmarks0(i);
    landmarks0_new(i)(3) = 1;
    landmarks1_new(i) = landmarks1(i);
    landmarks1_new(i)(3) = 1;
  
  }

  // Creating an overlay image
  std::string outFileName="before.h5";
  // To create an overlay image, find biggest dimentions
  int nLevels = zebrafish0.extent(0) < zebrafish1.extent(0) ? zebrafish1.extent(0) : zebrafish0.extent(0);
  int nRows   = zebrafish0.extent(1) < zebrafish1.extent(1) ? zebrafish1.extent(1) : zebrafish0.extent(1);
  int nCols   = zebrafish0.extent(2) < zebrafish1.extent(2) ? zebrafish1.extent(2) : zebrafish0.extent(2);

  blitz::Array< blitz::TinyVector< float, 3 >, 3> arr(nLevels, nRows, nCols);

  markLandmark(zebrafish0, landmarks0_new, element_size_um);
  markLandmark(zebrafish1, landmarks1_new, element_size_um);
  arr = overlay(zebrafish0, zebrafish1);
  writeBlitzToHDF5 (arr, "original", outFileName);
  writeBlitzTinyVectorToHDF5Attribute (element_size_um, "element_size_um", "/original", outFileName);

  

  // Transformation matrix
  blitz::TinyMatrix<float, 4, 4> Umeyama, H;
  Umeyama = umeyama(landmarks0_new, landmarks1_new);


  float sz = element_size_um(0);
  float sy = element_size_um(1);
  float sx = element_size_um(2);
  blitz::TinyMatrix<float, 4, 4> scaleToMicrometer;
  scaleToMicrometer = sz,  0,  0, 0,
                       0, sy,  0, 0,
                       0,  0, sx, 0,
                       0,  0,  0, 1;

  sz = 1.0/element_size_um(0);
  sy = 1.0/element_size_um(1);
  sx = 1.0/element_size_um(2);
  blitz::TinyMatrix<float, 4, 4>  scaleToVoxel;
  scaleToVoxel = sz,  0,  0, 0,
                  0, sy,  0, 0,
                  0,  0, sx, 0,
                  0,  0,  0, 1;


  H = myproduct(Umeyama, scaleToMicrometer);
  H = myproduct(scaleToVoxel, H);

  // Zebrafish0 - Static image
  // Zebrafish1 - Movin image (Transform it to Zebrafish0)
  blitz::Array<float, 3>  trgArr(zebrafish0.extent(0), zebrafish0.extent(1), zebrafish0.extent(2));
  transformArray (zebrafish1, H, trgArr);

  // Transform landmarks
  blitz::Array<blitz::TinyVector<double,4>, 1> landmarks_transformed(n_land);
  for (int i=0; i<landmarks0.extent(0); ++i){
  
    blitz::TinyVector< float, 4 > landmark;
    landmark = landmarks1_new(i);
    landmarks_transformed(i) = myproduct(Umeyama, landmark);
  
  }

  // Mark new landmarks
  markLandmark(trgArr, landmarks_transformed, element_size_um);

  // Create new overlay
  blitz::Array< blitz::TinyVector< float, 3 >, 3> transformed_overlay(nLevels, nRows, nCols);
  transformed_overlay = overlay(zebrafish0, trgArr);

  // Write output
  outFileName="after.h5";
  writeBlitzToHDF5 (transformed_overlay, "transformed", outFileName);
  writeBlitzTinyVectorToHDF5Attribute (element_size_um, "element_size_um", "/transformed", outFileName);

  cout << "Absolute distance before transformation: " << absoluteDistance(zebrafish0, zebrafish1) << endl;
  cout << "Absolute distance after transformation: " << absoluteDistance(zebrafish0, trgArr) << endl;

  return 0;

}

void transformArray (const blitz::Array<float, 3> &srcArr, 
		     const blitz::TinyMatrix<float, 4, 4> &invMat,
		     blitz::Array<float, 3>  &trgArr)
{

  blitz::TinyVector<float, 4> trgPos, srcPos;
  for (int lev = 0; lev < trgArr.extent(0); ++lev){ 
    for (int row = 0; row < trgArr.extent(1); ++row){ 
      for (int col = 0; col < trgArr.extent(2); ++col){ 

      	trgPos = lev, row, col, 1;
      	srcPos = myproduct  (invMat, trgPos);
      	trgArr (lev, row, col) = interpolNN (srcArr, srcPos);

      }
    }
  }

}

float interpolNN (const blitz::Array<float, 3> &arr, blitz::TinyVector<float, 4> pos)
{
  blitz::TinyVector<float, 4> new_pos_h = blitz::floor( pos + 0.5f ); 
  blitz::TinyVector<int, 3> new_pos;
  new_pos = new_pos_h(0), new_pos_h(1), new_pos_h(2);
  if ( blitz::all( new_pos >= 0 ) && blitz::all( new_pos < arr.shape() ) )
    return arr ( new_pos );
  else
    return 0;
}

void markLandmark ( blitz::Array<float, 3> &Image,
                    const blitz::Array<blitz::TinyVector<double, 4>, 1> &landmarks,
                    blitz::TinyVector<float, 3> element_size_um )
{

  int r = 5;
  blitz::TinyVector<double, 4> landmark; 
  landmark = 0;
  for (int i = 0; i < landmarks.extent(0); ++i)
  {

    landmark(0) = landmarks(i)(0)/element_size_um(0);
    landmark(1) = landmarks(i)(1)/element_size_um(1);
    landmark(2) = landmarks(i)(2)/element_size_um(2);
    landmark(3) = 1;
    int lev =  static_cast<int>(landmark(0)); 
    int row =  static_cast<int>(landmark(1)); 
    int col =  static_cast<int>(landmark(2));
    Image( blitz::Range(lev-r,lev+r), blitz::Range(row-r,row+r), blitz::Range(col-r,col+r) ) = 255;

  }

}

void mySVD( blitz::TinyMatrix<float, 3, 3> &A,
            blitz::TinyVector<float, 3> &S,
            blitz::TinyMatrix<float, 3, 3> &V)
{
  gsl_matrix *g_A  = gsl_matrix_calloc( 3, 3);
  gsl_matrix *g_V  = gsl_matrix_calloc( 3, 3);
  gsl_vector *g_S  = gsl_vector_calloc( 3 );
  gsl_vector *work = gsl_vector_calloc( 3 );

  
  // Set values for matrixes
  for (int row = 0; row < A.extent(0); ++row) 
  for (int col = 0; col < A.extent(1); ++col) 
  {
    gsl_matrix_set (g_A, row, col, A(row, col) );
    gsl_matrix_set (g_V, row, col, V(row, col) );

    gsl_vector_set (g_S, col,  S(col) );
  
  }

  // set the values in the matrix, e.g. with gsl_matrix_set (A, i, j, 1);
  gsl_linalg_SV_decomp( g_A, g_V, g_S, work );
 
  // Get back matrixes
  for (int row = 0; row < A.extent(0); ++row) 
  for (int col = 0; col < A.extent(1); ++col) 
  {
    A(row, col) = gsl_matrix_get (g_A, row, col);
    V(row, col) = gsl_matrix_get (g_V, row, col);

    S(row)      = gsl_vector_get (g_S, col);
  
  }

  // free the memory of the matrix
  gsl_matrix_free( g_A );
  gsl_matrix_free( g_V );
  gsl_vector_free( g_S );
}

blitz::Array< blitz::TinyVector<float, 3>, 3> overlay (const blitz::Array<float, 3> &fixedImage,
                                                       const blitz::Array<float, 3> &movingImage)
{
  int nLevels = fixedImage.extent(0) < movingImage.extent(0) ? movingImage.extent(0) : fixedImage.extent(0);
  int nRows   = fixedImage.extent(1) < movingImage.extent(1) ? movingImage.extent(1) : fixedImage.extent(1);
  int nCols   = fixedImage.extent(2) < movingImage.extent(2) ? movingImage.extent(2) : fixedImage.extent(2);
  
  cout << nLevels << " " << nRows << " " << nCols << endl;

  blitz::Array <blitz::TinyVector<float, 3>, 3> result(nLevels, nRows, nCols );
  result[0]( blitz::Range(0, fixedImage.extent(0)-1),
             blitz::Range(0, fixedImage.extent(1)-1),
             blitz::Range(0, fixedImage.extent(2)-1)) 

  = fixedImage(blitz::Range::all(),
               blitz::Range::all(),
               blitz::Range::all());

  result[1]( blitz::Range(0, movingImage.extent(0)-1),
             blitz::Range(0, movingImage.extent(1)-1),
             blitz::Range(0, movingImage.extent(2)-1)) 

  = movingImage(blitz::Range::all(),
               blitz::Range::all(),
               blitz::Range::all());

  return result;
}


blitz::TinyVector<float, 4> computeCOMOfLandmarks( const blitz::Array<blitz::TinyVector<double, 4>, 1> &landmarks)
{
  blitz::TinyVector<float,4> result;
  blitz::TinyVector<float, 4> landmark;
  
  int n = landmarks.extent(0);
  float sum_lev(0), sum_row(0), sum_col(0);
  for (int i=0; i<n; ++i)
  {

    landmark = landmarks(i);
    sum_lev += landmark(0);
    sum_row += landmark(1);
    sum_col += landmark(2);

  }
  result(0) = sum_lev/n;
  result(1) = sum_row/n;
  result(2) = sum_col/n;
  result(3) = 1;

  return result;
}

blitz::TinyMatrix<double, 3, 3> makeCovMatrix( const blitz::Array<blitz::TinyVector<double,4>, 1> X,
                                               const blitz::Array<blitz::TinyVector<double,4>, 1> Y)
{
  blitz::TinyMatrix<double,3,3> result;
  blitz::TinyMatrix<double,3,3> temp;

  result = 0;

  for (int k=0; k<11; ++k)
  {
    for (int j=0; j<3; ++j)
    for (int i=0; i<3; ++i)
    {
      result(j,i) += X(k)(i)*Y(k)(j);
    }
  }

  return result;
}

// ========= Umeyama method ========
blitz::TinyMatrix<float, 4, 4> umeyama( const blitz::Array<blitz::TinyVector<double, 4>, 1> &points,
                                        const blitz::Array<blitz::TinyVector<double, 4>, 1> &transformedPoints)
{

  // Demean
  blitz::TinyVector<double, 4> pos0, pos1;
  int n_land = points.extent(0);
  blitz::Array<blitz::TinyVector<double, 4>, 1> landmarks0_d(n_land), landmarks1_d(n_land);
  pos0 = computeCOMOfLandmarks(points);
  pos1 = computeCOMOfLandmarks(transformedPoints);
  for (int i=0; i<points.extent(0); ++i){
    landmarks0_d(i) = points(i) - pos0;
    landmarks1_d(i) = transformedPoints(i) - pos1;
  }

  // Singular value decomposition
  blitz::TinyMatrix<float, 3, 3> A;
  blitz::TinyVector<float, 3> S;
  blitz::TinyMatrix<float, 3, 3> V;

  // Construct covariance matrix
  A = makeCovMatrix(landmarks0_d,landmarks1_d);
  mySVD(A, S, V);

  V = mytranspose(V);

  // Compute rotation matrix
  blitz::TinyMatrix<float, 3, 3> R;
  R = myproduct(A, V);

  // Compute determinant of the rotation matrix
  float det;
  det =  R(0,0)*R(1,1)*R(2,2) + R(0,1)*R(1,2)*R(2,0) + R(2,1)*R(0,2)*R(1,0)
        -R(2,0)*R(1,1)*R(0,2) - R(1,0)*R(0,1)*R(2,2) - R(2,1)*R(1,2)*R(0,0);
  if(det == -1){
    blitz::TinyMatrix<float, 3, 3> S_m;
    S_m = 1, 0,  0,
          0, 1,  0,
          0, 0, -1;
    R = myproduct(S_m, V);
    R = myproduct(A, R);
  }

  // Construct transformation matrix
  blitz::TinyMatrix<float, 4, 4> H;

  // R to homogeneous coordinates
  blitz::TinyMatrix<float, 4, 4> rot_mat;
  rot_mat = R(0,0), R(0,1), R(0,2), 0,
            R(1,0), R(1,1), R(1,2), 0,
            R(2,0), R(2,1), R(2,2), 0,
                 0,      0,      0, 1;

  blitz::TinyMatrix<float, 4, 4> shiftTrgCenterToOrigin;
  shiftTrgCenterToOrigin = 1, 0, 0, -pos0(0),
                           0, 1, 0, -pos0(1),
                           0, 0, 1, -pos0(2),
                           0, 0, 0,       1;


  blitz::TinyMatrix<float, 4, 4> shiftOriginToScrCenter;
  shiftOriginToScrCenter = 1, 0, 0, pos1(0),
                           0, 1, 0, pos1(1),
                           0, 0, 1, pos1(2),
                           0, 0, 0,       1;



  H = myproduct(rot_mat, shiftTrgCenterToOrigin);
  H = myproduct(shiftOriginToScrCenter, H);

  return H;

}
