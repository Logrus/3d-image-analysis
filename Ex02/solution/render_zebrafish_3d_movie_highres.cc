//
//  Olaf Ronneberger
//  Sample Solution for Exercise 2 (Lecture "3D Image Analysis" Summer 2015) 
//
//  compile with:
//  g++ -Wall -O3 -g render_zebrafish_3d_movie_highres.cc -o render_zebrafish_3d_movie_highres
//
//  specify custom include directory with '-I' if blitz headers are not 
//  installed in '/usr/include'
//
//  requires: libblitz-0.10
//

#include <iostream>
#include <fstream>
#include <sstream>
#include <iomanip>

#include <blitz/array.h>

/*======================================================================*/
/*! 
 *   product of two 4x4 matrices
 *
 *   \param a  first matrix
 *   \param b  second matrix
 *
 *   \exception none
 *
 *   \return product of a and b
 */
/*======================================================================*/
inline
blitz::TinyMatrix<float,4,4> myproduct( 
    blitz::TinyMatrix<float,4,4> a, 
    blitz::TinyMatrix<float,4,4> b)
{
  blitz::TinyMatrix<float,4,4> out;
  out(0,0) = a(0,0)*b(0,0) + a(0,1)*b(1,0) + a(0,2)*b(2,0) + a(0,3)*b(3,0);
  out(1,0) = a(1,0)*b(0,0) + a(1,1)*b(1,0) + a(1,2)*b(2,0) + a(1,3)*b(3,0);
  out(2,0) = a(2,0)*b(0,0) + a(2,1)*b(1,0) + a(2,2)*b(2,0) + a(2,3)*b(3,0);
  out(3,0) = a(3,0)*b(0,0) + a(3,1)*b(1,0) + a(3,2)*b(2,0) + a(3,3)*b(3,0);
  
  out(0,1) = a(0,0)*b(0,1) + a(0,1)*b(1,1) + a(0,2)*b(2,1) + a(0,3)*b(3,1);
  out(1,1) = a(1,0)*b(0,1) + a(1,1)*b(1,1) + a(1,2)*b(2,1) + a(1,3)*b(3,1);
  out(2,1) = a(2,0)*b(0,1) + a(2,1)*b(1,1) + a(2,2)*b(2,1) + a(2,3)*b(3,1);
  out(3,1) = a(3,0)*b(0,1) + a(3,1)*b(1,1) + a(3,2)*b(2,1) + a(3,3)*b(3,1);
  
  out(0,2) = a(0,0)*b(0,2) + a(0,1)*b(1,2) + a(0,2)*b(2,2) + a(0,3)*b(3,2);
  out(1,2) = a(1,0)*b(0,2) + a(1,1)*b(1,2) + a(1,2)*b(2,2) + a(1,3)*b(3,2);
  out(2,2) = a(2,0)*b(0,2) + a(2,1)*b(1,2) + a(2,2)*b(2,2) + a(2,3)*b(3,2);
  out(3,2) = a(3,0)*b(0,2) + a(3,1)*b(1,2) + a(3,2)*b(2,2) + a(3,3)*b(3,2);
  
  out(0,3) = a(0,0)*b(0,3) + a(0,1)*b(1,3) + a(0,2)*b(2,3) + a(0,3)*b(3,3);
  out(1,3) = a(1,0)*b(0,3) + a(1,1)*b(1,3) + a(1,2)*b(2,3) + a(1,3)*b(3,3);
  out(2,3) = a(2,0)*b(0,3) + a(2,1)*b(1,3) + a(2,2)*b(2,3) + a(2,3)*b(3,3);
  out(3,3) = a(3,0)*b(0,3) + a(3,1)*b(1,3) + a(3,2)*b(2,3) + a(3,3)*b(3,3);
  
  return out;
}
 

/*======================================================================*/
/*! 
 *   product of 4x4 matrix with 4d vector
 *
 *   \param m  the matrix
 *   \param v  the vector
 *
 *   \exception none
 *
 *   \return  product of m and v
 */
/*======================================================================*/
inline
blitz::TinyVector<float,4> myproduct( blitz::TinyMatrix<float,4,4> m, 
                                      blitz::TinyVector<float,4> v)
{
  blitz::TinyVector<float,4> out;
  out(0) = m(0,0)*v(0) + m(0,1)*v(1) + m(0,2)*v(2) + m(0,3)*v(3);
  out(1) = m(1,0)*v(0) + m(1,1)*v(1) + m(1,2)*v(2) + m(1,3)*v(3);
  out(2) = m(2,0)*v(0) + m(2,1)*v(1) + m(2,2)*v(2) + m(2,3)*v(3);
  out(3) = m(3,0)*v(0) + m(3,1)*v(1) + m(3,2)*v(2) + m(3,3)*v(3);
  
  return out;
}


/*======================================================================*/
/*! 
 *   Nearest neighbour interpolation with boundary check. For positions
 *   outside the array gray value 0 is returned
 *
 *   \param arr the array
 *   \param pos float position
 *
 *   \exception  none
 *
 *   \return "interpolated" gray value at pos
 */
/*======================================================================*/
inline
unsigned char interpolNN( const blitz::Array<unsigned char, 3>& arr,
                          blitz::TinyVector<float,4> pos)
{
  // normalize homogeneous coordinates if necessary
  if(pos(3) != 1) pos /= pos(3);
  
  // find nearest neighbour
  blitz::TinyVector<int,4> ipos;
  ipos = blitz::floor( pos + 0.5);
  if( blitz::all(ipos >= 0) && blitz::all(ipos < arr.shape()))
  {
    return arr(ipos);
  }
  else
  {
    return 0;
  }
}

/*======================================================================*/
/*! 
 *   Linear interpolation with boundary check. For positions
 *   outside the array gray value 0 is returned
 *
 *   \param arr the array
 *   \param pos float position
 *
 *   \exception  none
 *
 *   \return "interpolated" gray value at pos
 */
/*======================================================================*/
template<typename T>
inline
T interpolBiLin( const blitz::Array<T, 3>& arr,
                        blitz::TinyVector<float,4> pos)
{
  // normalize homogeneous coordinates if necessary
  if(pos(3) != 1) pos /= pos(3);
  
  // find nearest neighbour
  blitz::TinyVector<int,4> ipos;
  ipos = blitz::floor( pos);
  if( blitz::all(ipos >= 0) && blitz::all(ipos+1 < arr.shape()))
  {
    blitz::TinyVector<float,4> fpos;
    fpos = pos - ipos;
    return 
          (1-fpos(0)) * (1-fpos(1)) * (1-fpos(2)) * arr( ipos(0)  , ipos(1)  , ipos(2)  )
        + (1-fpos(0)) * (1-fpos(1)) * (  fpos(2)) * arr( ipos(0)  , ipos(1)  , ipos(2)+1)
        + (1-fpos(0)) * (  fpos(1)) * (1-fpos(2)) * arr( ipos(0)  , ipos(1)+1, ipos(2)  )
        + (1-fpos(0)) * (  fpos(1)) * (  fpos(2)) * arr( ipos(0)  , ipos(1)+1, ipos(2)+1)
        + (  fpos(0)) * (1-fpos(1)) * (1-fpos(2)) * arr( ipos(0)+1, ipos(1)  , ipos(2)  )
        + (  fpos(0)) * (1-fpos(1)) * (  fpos(2)) * arr( ipos(0)+1, ipos(1)  , ipos(2)+1)
        + (  fpos(0)) * (  fpos(1)) * (1-fpos(2)) * arr( ipos(0)+1, ipos(1)+1, ipos(2)  )
        + (  fpos(0)) * (  fpos(1)) * (  fpos(2)) * arr( ipos(0)+1, ipos(1)+1, ipos(2)+1);
    
  }
  else
  {
    return 0;
  }
}

/*======================================================================*/
/*! 
 *   transform array with given 4x4 Matrix using homogeneous
 *   coordinates using nearest neighbour interpolation
 *
 *   \param srcArr  the source Array
 *   \param invMat  the inverse transformation matrix. Will be used to
 *                  project trg coordinates (homogeneous coordinates
 *                  in blitz-format, i.e. (lev,row,col,1) ) to src
 *                  coordinates.
 *   \param trgArr  the target Array -- must already have the wanted
 *                  size 
 *
 *   \exception none
 *
 *   \return none
 */
/*======================================================================*/
void transformArray( const blitz::Array<unsigned char, 3>& srcArr,
                     const blitz::TinyMatrix<float,4,4>& invMat,
                     blitz::Array<unsigned char, 3>& trgArr)
{
  blitz::TinyVector<float,4> srcPos;
  blitz::TinyVector<float,4> trgPos;
  
  for( int lev = 0; lev < trgArr.extent(0); ++lev)
  {
    for( int row = 0; row < trgArr.extent(1); ++row)
    {
      for( int col = 0; col < trgArr.extent(2); ++col)
      {
        trgPos = lev, row, col, 1;
        srcPos = myproduct( invMat, trgPos);
        trgArr(lev,row,col) = interpolBiLin( srcArr, srcPos);
      }
    }
  }
}


/*======================================================================*/
/*! 
 *   creates an inverse rotation matrix for homogenous coordinates of
 *   the form (lev,row,col,1), to be used as 
 *
 *      srcPosition = Matrix * trgPosition
 *
 *   The translation is adjusted such that the center of the target
 *   array is mapped to the center of the source array
 *
 *   \param srcArrShape     			shape of source array (nLevels, nRows, nCols)
 *   \param trgArrShape     			shape of target array (nLevels, nRows, nCols)
 *	 \param src_element_size_um		voxel sizes of source array in micrometer (level, row, col)
 *	 \param trg_element_size_um		voxel sizes of target array in micrometer (level, row, col)
 *   \param angleAroundLev  			rotation angle around level-axis in degrees
 *   \param angleAroundRow  			rotation angle around row-axis in degrees
 *   \param angleAroundCol  			rotation angle around column-axis in degrees
 *
 *   \exception none
 *
 *   \return the matrix
 */
/*======================================================================*/

blitz::TinyMatrix<float,4,4> createInverseRotationMatrix(
    const blitz::TinyVector<size_t, 3>& srcArrShape,
    const blitz::TinyVector<size_t, 3>& trgArrShape,
    const blitz::TinyVector<float,3>& src_element_size_um,
    const blitz::TinyVector<float,3>& trg_element_size_um,
    float angleAroundLev,
    float angleAroundRow,
    float angleAroundCol)
{  
  blitz::TinyMatrix<float,4,4> shiftTrgCenterToOrigin, scaleToMicrometer, rotateAroundLev, rotateAroundRow, rotateAroundCol, scaleToVoxel, shiftOriginToSrcCenter;

  shiftTrgCenterToOrigin = 
      1, 0, 0, -float(trgArrShape(0))/2,
      0, 1, 0, -float(trgArrShape(1))/2,
      0, 0, 1, -float(trgArrShape(2))/2,
      0, 0, 0, 1;
  

  scaleToMicrometer = 
      trg_element_size_um(0), 0, 0, 0,
      0, trg_element_size_um(1), 0, 0, 
      0, 0, trg_element_size_um(2), 0, 
      0, 0, 0,                      1;


  float sina = sin( -angleAroundLev / 180 * M_PI);
  float cosa = cos( -angleAroundLev / 180 * M_PI);
  rotateAroundLev = 
      1,   0,     0,    0, 
      0,   cosa, -sina, 0,
      0,   sina, cosa,  0,
      0,   0,     0,    1;
  

  sina = sin( -angleAroundRow / 180 * M_PI);
  cosa = cos( -angleAroundRow / 180 * M_PI);
  rotateAroundRow = 
      cosa,  0,   sina, 0, 
      0,     1,   0,    0,
      -sina, 0,   cosa, 0,
      0,     0,     0,  1;


  sina = sin( -angleAroundCol / 180 * M_PI);
  cosa = cos( -angleAroundCol / 180 * M_PI);
  rotateAroundCol = 
      cosa, -sina, 0, 0,
      sina, cosa,  0, 0, 
      0,    0,     1, 0,
      0,    0,     0, 1;
  

  scaleToVoxel = 
      1/src_element_size_um(0), 0, 0, 0,
      0, 1/src_element_size_um(1), 0, 0,
      0, 0, 1/src_element_size_um(2), 0,
      0, 0, 0,                        1;  


  shiftOriginToSrcCenter = 
      1, 0, 0, float(srcArrShape(0))/2,
      0, 1, 0, float(srcArrShape(1))/2,
      0, 0, 1, float(srcArrShape(2))/2,
      0, 0, 0, 1;
  
  
  blitz::TinyMatrix<float,4,4> resultMatrix;
  
  resultMatrix = shiftTrgCenterToOrigin;
  resultMatrix = myproduct( scaleToMicrometer,      resultMatrix);
  resultMatrix = myproduct( rotateAroundLev,        resultMatrix);
  resultMatrix = myproduct( rotateAroundRow,        resultMatrix);
  resultMatrix = myproduct( rotateAroundCol,        resultMatrix);
  resultMatrix = myproduct( scaleToVoxel,           resultMatrix);
  resultMatrix = myproduct( shiftOriginToSrcCenter, resultMatrix);

  return resultMatrix;
}


/*======================================================================*/
/*! 
 *   save color image in PPM (portable pix map) format. If an error
 *   occures, the program is terminated
 *
 *   \param image the image
 *   \param fileName the file Name (e.g. "test.ppm")
 *
 *   \exception none
 *
 *   \return none
 */
/*======================================================================*/
void savePPMImage( 
    const blitz::Array<blitz::TinyVector<unsigned char,3>,2>& image,
    const std::string& fileName)
{
  std::ofstream outFile( fileName.c_str(), std::ofstream::binary);
  if( !outFile)
  {
    std::cerr << "Can't create '" << fileName << "'";
    perror("");
    exit(1);
  }
  
  outFile << "P6\n"
          << image.extent(1) << " " << image.extent(0) << " 255\n";
  outFile.write( reinterpret_cast<const char*>(image.dataFirst()), 
                 image.size() * sizeof( blitz::TinyVector<unsigned char,3>));

}


int main( int argc, char**argv)
{

  /*-----------------------------------------------------------------------
   *  Load the data set "Zebrafish_71x361x604_8bit.raw".
   *-----------------------------------------------------------------------*/ 
  int nLevels = 71;
  int nRows = 361;
  int nCols = 604;
  
  blitz::Array<unsigned char,3> srcArr( nLevels, nRows, nCols);
  std::ifstream inFile( "Zebrafish_71x361x604_8bit.raw", 
                        std::ifstream::binary);
  
  if( !inFile)
  {
    perror( "Can't open 'Zebrafish_71x361x604_8bit.raw'");
    exit(1);
  }
  
  inFile.read( reinterpret_cast<char*>( srcArr.dataFirst()), 
               srcArr.size() * sizeof( char));
  
  /*-----------------------------------------------------------------------
   *  Load the data set "Zebrafish_71x361x604_8bit.raw".
   *-----------------------------------------------------------------------*/
  const blitz::TinyVector<float,3> src_element_size_um(3.9, 1.49155, 1.49155);
  const blitz::TinyVector<float,3> trg_element_size_um(3.9, 1.49155, 1.49155);

  /*-----------------------------------------------------------------------
   *  Create the target Array (must be larger than the source Array,
   *  such that the 45 degree rotated array fits into it)
   *-----------------------------------------------------------------------*/
  int diagLevCol = (int) ceil(sqrt( srcArr.extent(0)*srcArr.extent(0) + 
                              srcArr.extent(2)*srcArr.extent(2)));
  blitz::Array<unsigned char,3> trgArr( diagLevCol, srcArr.extent(1), diagLevCol);
 
  /*-----------------------------------------------------------------------
   *  Create Anaglyph-image
   *-----------------------------------------------------------------------*/

  const float stereoAngle = 2.5;	// angle in degrees

  blitz::Array<unsigned char,2> singleLeftView( trgArr.extent(1), trgArr.extent(2));
  blitz::Array<unsigned char,2> singleRightView( trgArr.extent(1), trgArr.extent(2));

  blitz::TinyMatrix<float,4,4> rotMatRowLeft;
  blitz::TinyMatrix<float,4,4> rotMatRowRight;

  /*---------------------------------------------------------------------
   *  compute image for the right eye, rotated array (by +stereoAngle/2 around row)
   *---------------------------------------------------------------------*/
  rotMatRowLeft = createInverseRotationMatrix( srcArr.shape(), trgArr.shape(),
						src_element_size_um, trg_element_size_um,
						0, stereoAngle/2, 0);
  
  transformArray( srcArr, rotMatRowLeft, trgArr);
  
  /*---------------------------------------------------------------------
   *  compute MIP of rotated array
   *---------------------------------------------------------------------*/
  singleRightView = 0;
  for( int lev = 0; lev < trgArr.extent(0); ++lev)
  {
    singleRightView = blitz::max( singleRightView, trgArr( lev,
							   blitz::Range::all(),
							   blitz::Range::all()));
  }

  /*---------------------------------------------------------------------
   *  compute image for the left eye, rotated array (by -stereoAngle/2 around row)
   *---------------------------------------------------------------------*/
  rotMatRowRight = createInverseRotationMatrix( srcArr.shape(), trgArr.shape(),
						src_element_size_um, trg_element_size_um,
						0, -stereoAngle/2, 0);
  
  transformArray( srcArr, rotMatRowRight, trgArr);
  
  /*---------------------------------------------------------------------
   *  compute MIP of rotated array
   *---------------------------------------------------------------------*/
  singleLeftView = 0;
  for( int lev = 0; lev < trgArr.extent(0); ++lev)
  {
    singleLeftView = blitz::max( singleLeftView, trgArr( lev,
							 blitz::Range::all(),
							 blitz::Range::all()));
  }
 
  /*---------------------------------------------------------------------
   *  copy into red-cyan anaglyhp image
   *---------------------------------------------------------------------*/
  blitz::Array< blitz::TinyVector<unsigned char,3> ,2> singleAnaglypgImage( trgArr.extent(1), trgArr.extent(2));

  singleAnaglypgImage[0] = singleLeftView;
  singleAnaglypgImage[1] = singleRightView;
  singleAnaglypgImage[2] = singleRightView;

  /*---------------------------------------------------------------------
   *  save the resulting image to "anaglyphImage.ppm"
   *---------------------------------------------------------------------*/
  std::ostringstream os;
  os << "anaglyphImage.ppm";
  std::string fileName = os.str();
  std::cout << "saving " << fileName << std::endl;
  
  savePPMImage( singleAnaglypgImage, fileName);


  /*-----------------------------------------------------------------------
   *  Create Anaglyph-movie
   *-----------------------------------------------------------------------*/
  blitz::TinyMatrix<float,4,4> rotMatColRowLeft;
  blitz::TinyMatrix<float,4,4> rotMatColRowRight;

  /*-----------------------------------------------------------------------
   *  Create the target Array (must be larger than the source Array,
   *  such that the 45 degree rotated array fits into it)
   *-----------------------------------------------------------------------*/
  int diagLevRow = (int) ceil(sqrt( srcArr.extent(0)*srcArr.extent(0) + 
                              srcArr.extent(1)*srcArr.extent(1)));

  blitz::Array<unsigned char,3> trgArrAnaglyph( std::max(diagLevRow,diagLevCol), diagLevRow, diagLevCol);

  blitz::Array<unsigned char,2> leftView( trgArrAnaglyph.extent(1), trgArrAnaglyph.extent(2));
  blitz::Array<unsigned char,2> rightView( trgArrAnaglyph.extent(1), trgArrAnaglyph.extent(2));

  // define target image (half) size
  int trg_nRows = 360;
  int trg_nCols = 640;
  // define target over/under image
  blitz::Array< blitz::TinyVector<unsigned char,3> ,2> overUnderImage( 2*trg_nRows, trg_nCols);
  // define source image size
  int src_nRows = trgArrAnaglyph.extent(1);
  int src_nCols = trgArrAnaglyph.extent(2);
  
  blitz::TinyVector<int,2> trg_center(trg_nRows/2, trg_nCols/2);
  blitz::TinyVector<int,2> src_center(src_nRows/2, src_nCols/2);
  int min_nRows = std::min(src_nRows,trg_nRows);
  int min_nCols = std::min(src_nCols,trg_nCols);
  
  /*-----------------------------------------------------------------------
   *  create an anaglyph-movie with 1 angle degree steps
   *-----------------------------------------------------------------------*/
  for( int angle = 0; angle < 360; angle += 1)
  {
    /*---------------------------------------------------------------------
     *  compute image for the right eye, rotated array (by alpha around col, by +stereoAngle/2 around row)
     *---------------------------------------------------------------------*/
    rotMatColRowLeft = createInverseRotationMatrix( srcArr.shape(), trgArrAnaglyph.shape(),
						    src_element_size_um, trg_element_size_um,
						    0, stereoAngle/2, angle);
    
    transformArray( srcArr, rotMatColRowLeft, trgArrAnaglyph);
    
    /*---------------------------------------------------------------------
     *  compute MIP of rotated array
     *---------------------------------------------------------------------*/
    rightView = 0;
    for( int lev = 0; lev < trgArrAnaglyph.extent(0); ++lev)
    {
      rightView = blitz::max( rightView, trgArrAnaglyph( lev,
							 blitz::Range::all(),
							 blitz::Range::all()));
    }

    /*---------------------------------------------------------------------
     *  compute image for the left eye, rotated array (by alpha around col, by -stereoAngle/2 around row)
     *---------------------------------------------------------------------*/
    rotMatColRowRight = createInverseRotationMatrix( srcArr.shape(), trgArrAnaglyph.shape(),
						     src_element_size_um, trg_element_size_um,
						     0, -stereoAngle/2, angle);
    
    transformArray( srcArr, rotMatColRowRight, trgArrAnaglyph);
    
    /*---------------------------------------------------------------------
     *  compute MIP of rotated array
     *---------------------------------------------------------------------*/
    leftView = 0;
    for( int lev = 0; lev < trgArrAnaglyph.extent(0); ++lev)
    {
      leftView = blitz::max( leftView, trgArrAnaglyph( lev,
						       blitz::Range::all(),
						       blitz::Range::all()));
    }
   
    /*---------------------------------------------------------------------
     *  copy into over/under-formatted image
     *---------------------------------------------------------------------*/      
    blitz::Array<unsigned char,2> trg_leftView(trg_nRows, trg_nCols);
    blitz::Array<unsigned char,2> trg_rightView(trg_nRows, trg_nCols);
    trg_leftView = 0;
    trg_rightView = 0;
    
    // define crop ranges
    blitz::TinyVector<int,4> trgRange(std::max(trg_center(0)-min_nRows/2, 0), std::min(trg_center(0)-min_nRows/2+min_nRows-1, trg_nRows-1), 
					std::max(trg_center(1)-min_nCols/2, 0), std::min(trg_center(1)-min_nCols/2+min_nCols-1, trg_nCols-1));
				
    blitz::TinyVector<int,4> srcRange(std::max(src_center(0)-min_nRows/2, 0), std::min(src_center(0)-min_nRows/2+min_nRows-1, src_nRows-1),
					std::max(src_center(1)-min_nCols/2, 0), std::min(src_center(1)-min_nCols/2+min_nCols-1, src_nCols-1));
    
    // create over/under image	  
    trg_leftView(blitz::Range(trgRange(0),trgRange(1)), blitz::Range(trgRange(2),trgRange(3))) = 
	      leftView(blitz::Range(srcRange(0),srcRange(1)), blitz::Range(srcRange(2),srcRange(3)));
    
    trg_rightView(blitz::Range(trgRange(0),trgRange(1)), blitz::Range(trgRange(2),trgRange(3))) =
	      rightView(blitz::Range(srcRange(0),srcRange(1)), blitz::Range(srcRange(2),srcRange(3)));
        
    // over image (left view)
    overUnderImage[0](blitz::Range(0,trg_nRows-1), blitz::Range::all()) = trg_leftView;
    overUnderImage[1](blitz::Range(0,trg_nRows-1), blitz::Range::all()) = trg_leftView;
    overUnderImage[2](blitz::Range(0,trg_nRows-1), blitz::Range::all()) = trg_leftView;
    // under image (right view)
    overUnderImage[0](blitz::Range(trg_nRows,2*trg_nRows-1), blitz::Range::all()) = trg_rightView;
    overUnderImage[1](blitz::Range(trg_nRows,2*trg_nRows-1), blitz::Range::all()) = trg_rightView;
    overUnderImage[2](blitz::Range(trg_nRows,2*trg_nRows-1), blitz::Range::all()) = trg_rightView;
      
    /*---------------------------------------------------------------------
     *  save the resulting image to "movie_3d_frame_XXX.ppm", where
     *  XXX is the (zeropadded) angle
     *---------------------------------------------------------------------*/
    std::ostringstream os;
    os << "3dmovie/movie_3d_frame_" 
       << std::setw(3) << std::setfill('0') << angle 
       << ".ppm";
    std::string fileName = os.str();
    std::cout << "saving " << fileName << std::endl;
    
    savePPMImage( overUnderImage, fileName);
    
  }
  

  return 0;
}

