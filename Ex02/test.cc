//
//  Olaf Ronneberger
//  Sample Solution for Exercise 2 (Lecture "3D Image Analysis" Summer 2014) 
//
//  compile with:
//  g++ -Wall -g test.cc -o test -lblitz
//
//  or this if it complains about paths:
//  g++ -Wall -g test.cc -o test -I/usr/include -L/usr/lib -lblitz


#include <iostream>
#include <fstream>
#include <blitz/array.h>

int main( int argc, char** argv)
{

  /*-----------------------------------------------------------------------
   *  test program: Blitz++ arrays
   *-----------------------------------------------------------------------*/

	// set array dimensions
	int nLevels = 41;
	int nRows = 51;
	int nCols = 61;

	// create blitz++ array, 3d
	blitz::Array<float,3> out( nLevels, nRows, nCols);
	out = 0.0;

	for  (int lev = 0; lev < out.extent(0); ++lev)
	{
		for (int row = 0; row < out.extent(1); ++row)
		{
			for (int col = 0; col < out.extent(2); ++col)
			{

				// here, write whatever you what to see in the 3d array later
				// use the following code example to access the 3d array:

				float value;
				value = 1.0;
				out(lev,row,col) = value; 	

				// hint: This would give a boring output ;)

			}
		}
	}

  /*-----------------------------------------------------------------------
   *  save the array, in raw format
   *-----------------------------------------------------------------------*/
  blitz::Array<unsigned char,3> arr( nLevels, nRows, nCols);
  arr = blitz::cast<unsigned char>( out);
  
  std::ofstream outFile( "test_41x51x61_8bit.raw", std::ofstream::binary);
  outFile.write( reinterpret_cast<char*>(arr.dataFirst()), 
                 arr.size()*sizeof( unsigned char));


  return 0;
}
