// =====================================================
//               RENDER ANAGLYPH MOVIE
// =====================================================
#include <iostream>
#include <fstream>
#include <sstream>
#include <iomanip>
#include <blitz/array.h>
#include <string>

blitz::TinyMatrix<float, 4, 4> myproduct (blitz::TinyMatrix<float, 4, 4> a, blitz::TinyMatrix<float, 4, 4> b);
blitz::TinyVector<float, 4> myproduct (blitz::TinyMatrix<float, 4, 4> m, blitz::TinyVector<float, 4> v);

unsigned char interpolNN (const blitz::Array<unsigned char, 3> &arr, blitz::TinyVector<float, 4> pos);

void transformArray (const blitz::Array<unsigned char, 3> &srcArr, 
		     const blitz::TinyMatrix<float, 4, 4> &invMat,
		     blitz::Array<unsigned char, 3>  &trgArr);

blitz::TinyMatrix<float, 4, 4> createInverseRotationMatrix (const blitz::TinyVector<size_t, 3> &srcArrShape,
				const blitz::TinyVector<size_t, 3> &trgArrShape,
				const blitz::TinyVector<float, 3> &src_element_size_um,
				const blitz::TinyVector<float, 3> &trg_element_size_um,
				float angleAroundLev,
				float angleAroundRow,
				float angleAroundCol);

void savePGMImage (const blitz::Array<unsigned char, 2> &Image, const std::string &fileName);

blitz::Array<unsigned char, 2> maxIntProj (const blitz::Array<unsigned char, 3> &srcArr);

blitz::Array<unsigned char, 2> rotateArr(const blitz::Array<unsigned char, 3> &srcArr,
                                         float levAngle,
                                         float rowAngle,
                                         float colAngle);

void savePPMImage( const blitz::Array<blitz::TinyVector<unsigned char,3>,2>& image,
                   const std::string& fileName);

int main (int argc, char** argv)
{
 
  // Define dataset properties
  const char* filename = "Artemisia_pollen_71x136x136_8bit.raw";
  int nLevels = 71;
  int nRows = 136;
  int nCols = 136;
  
  // Create target array
  blitz::Array<unsigned char, 3> srcArr(nLevels, nRows, nCols);

  // Load data
  std::ifstream file (filename, std::ifstream::in | std::ifstream::binary );
  file.read (reinterpret_cast<char*>(srcArr.data()), srcArr.size()*sizeof(char));
 
  int x = ceil ( sqrt( nLevels*nLevels + nCols*nCols) );
  float stereoAngle = 5;

  // Make anaglyph image
  blitz::Array<unsigned char, 2> leftView(nRows, x); 
  blitz::Array<unsigned char, 2> rightView(nRows, x); 
  leftView = 0;
  rightView = 0;
  leftView  = rotateArr( srcArr, 0,  stereoAngle/2, 0);
  rightView = rotateArr( srcArr, 0, -stereoAngle/2, 0);
  blitz::Array< blitz::TinyVector<unsigned char, 3>, 2> resultingImage( leftView.extent(0),
                                                                        leftView.extent(1));

  resultingImage[0] = leftView;
  resultingImage[1] = rightView;
  resultingImage[2] = rightView;
  std::string test = "test.ppm";

  savePPMImage( resultingImage, test);


  for (int angle = 0; angle < 355; angle += 5)
  {

    leftView  = rotateArr(srcArr, 0, angle+stereoAngle/2, 0);
    rightView = rotateArr(srcArr, 0, angle-stereoAngle/2, 0);

    // Save output
    resultingImage[0] = leftView;
    resultingImage[1] = rightView;
    resultingImage[2] = rightView;
    std::ostringstream filename;
    filename << "test_" << std::setw(3) << std::setfill('0') << angle << ".pgm";
    savePPMImage( resultingImage, filename.str());

 }

  return 0;
}


blitz::TinyMatrix<float, 4, 4> myproduct (blitz::TinyMatrix<float, 4, 4> a, blitz::TinyMatrix<float, 4, 4> b)
{
  blitz::TinyMatrix<float, 4, 4> result;
  float sum (0); 
  for (int lev = 0; lev < a.extent (0); ++lev) {
    for (int row = 0; row < a.extent (0); ++row) {
      for (int col = 0; col < a.extent (1); ++col) {
	sum = sum + a (lev, col) * b (col, row);
      }
      result (lev,row) = sum;
      sum = 0;
    }
  }
  return result;
}


blitz::TinyVector<float, 4> myproduct (blitz::TinyMatrix<float, 4, 4> m, blitz::TinyVector<float, 4> v)
{
  blitz::TinyVector<float, 4> result;

  float sum (0);

  for (int row = 0; row < m.extent (0); ++row) {
    for (int col = 0; col < m.extent (1); ++col) {
      sum = sum + m (row, col) * v (col);
    }
    result (row) = sum;
    sum = 0;
  }

  return result;
}

unsigned char interpolNN (const blitz::Array<unsigned char, 3> &arr, blitz::TinyVector<float, 4> pos)
{
  blitz::TinyVector<float, 4> new_pos = blitz::floor( pos + 0.5f ); 
  if ( blitz::all( new_pos >= 0 ) && blitz::all( new_pos < arr.shape() ) )
    return arr ( new_pos );
  else
    return 0;
}

void transformArray (const blitz::Array<unsigned char, 3> &srcArr, 
		     const blitz::TinyMatrix<float, 4, 4> &invMat,
		     blitz::Array<unsigned char, 3>  &trgArr)
{
  //std::cout << trgArr.extent(2) << std::endl;
  blitz::TinyVector<float, 4> trgPos, srcPos;
  for (int lev = 0; lev < trgArr.extent(0); ++lev){ 
    for (int row = 0; row < trgArr.extent(1); ++row){ 
      for (int col = 0; col < trgArr.extent(2); ++col){ 

	trgPos = lev, row, col, 1;
	srcPos = myproduct (invMat, trgPos);
	trgArr (lev, row, col) = interpolNN (srcArr, srcPos);

      }
    }
  }

}

blitz::TinyMatrix<float, 4, 4> createInverseRotationMatrix (const blitz::TinyVector<size_t, 3> &srcArrShape,
				const blitz::TinyVector<size_t, 3> &trgArrShape,
				const blitz::TinyVector<float, 3> &src_element_size_um,
				const blitz::TinyVector<float, 3> &trg_element_size_um,
				float angleAroundLev,
				float angleAroundRow,
				float angleAroundCol)
{
  blitz::TinyMatrix<float, 4, 4> result;
  
  // Definition of the seven matrixes
  blitz::TinyMatrix<float, 4, 4> shiftTrgCenterToOrigin, scaleToMicrometer, rotateAroundLev, rotateAroundRow, rotateAroundCol;
  blitz::TinyMatrix<float, 4, 4> scaleToVoxel, shiftOriginToScrCenter;
  
  shiftTrgCenterToOrigin = 1, 0, 0, -float(trgArrShape(0))/2,
			   0, 1, 0, -float(trgArrShape(1))/2,
			   0, 0, 1, -float(trgArrShape(2))/2,
			   0, 0, 0, 1;

  float sx = src_element_size_um(0);
  float sy = src_element_size_um(1);
  float sz = src_element_size_um(2);
  scaleToMicrometer = sx,  0,  0, 0,
		       0, sy,  0, 0,
		       0,  0, sz, 0,
		       0,  0,  0, 1;

  float a = angleAroundCol;
  rotateAroundCol = cos(a), -sin(a), 0, 0,
		    sin(a),  cos(a), 0, 0,
		         0,       0, 1, 0,
		         0,       0, 0, 1;

  a = angleAroundRow;
  rotateAroundRow =   cos(a),   0,  sin(a), 0,
		           0,   1,       0, 0,
		     -sin(a),   0,  cos(a), 0,
		           0,   0,       0, 1;

  a = angleAroundLev;
  rotateAroundLev = 1,      0,       0, 0, 
                    0, cos(a), -sin(a), 0,
		    0, sin(a),  cos(a), 0,
		    0,      0,       0, 1;

  sx = 1.0/trg_element_size_um(0);
  sy = 1.0/trg_element_size_um(1);
  sz = 1.0/trg_element_size_um(2);
  scaleToVoxel = sx,  0,  0, 0,
		  0, sy,  0, 0,
		  0,  0, sz, 0,
		  0,  0,  0, 1;

  shiftOriginToScrCenter = 1, 0, 0, float(srcArrShape(0))/2,
			   0, 1, 0, float(srcArrShape(1))/2,
			   0, 0, 1, float(srcArrShape(2))/2,
			   0, 0, 0, 1;

  result = myproduct(scaleToMicrometer, shiftTrgCenterToOrigin);
  result = myproduct(rotateAroundCol, result);
  result = myproduct(rotateAroundRow, result);
  result = myproduct(rotateAroundLev, result);
  result = myproduct(scaleToVoxel, result);
  result = myproduct(shiftOriginToScrCenter, result);
  
  return result;
}


void savePGMImage (const blitz::Array<unsigned char, 2> &Image, const std::string &fileName)
{
  std::ofstream outf (fileName.c_str());
  outf << "P5\n" << Image.extent(1) << " " << Image.extent(0) << "\n255\n";
  outf.write (reinterpret_cast<const char*>(Image.dataFirst()), Image.size()*sizeof(char));
  outf.close();
}


blitz::Array<unsigned char, 2> maxIntProj (const blitz::Array<unsigned char, 3> &srcArr)
{
  blitz::Array<unsigned char, 2> image(srcArr.extent(1),srcArr.extent(2));
  image = 0;

  for (int lev = 0; lev < srcArr.extent(0); lev++)
    for (int row = 0; row < srcArr.extent(1); row++)
      for (int col = 0; col < srcArr.extent(2); col++){
  	if (image(row,col) < srcArr(lev, row, col)){
  	  image(row, col) = srcArr(lev, row, col); 
 	}
     }
  return image; 
}


blitz::Array<unsigned char, 2> rotateArr(const blitz::Array<unsigned char, 3> &srcArr,
                                         float levAngle,
                                         float rowAngle,
                                         float colAngle)
{

 float levA = (3.14*levAngle)/180; 
 float rowA = (3.14*rowAngle)/180; 
 float colA = (3.14*colAngle)/180; 

 // Set voxel scale  
 blitz::TinyVector<float, 3> src_element_size_um(0.4, 0.2, 0.2); 
 blitz::TinyVector<float, 3> trg_element_size_um(0.4, 0.2, 0.2); 


  
  int x = ceil ( sqrt ( srcArr.extent(2)*srcArr.extent(2) + 
                        srcArr.extent(0)*srcArr.extent(0) ));

  blitz::Array<unsigned char, 3> trgArr(x, srcArr.extent(1), x);

  // Record images with rotations
  blitz::TinyMatrix<float, 4, 4> invMat = createInverseRotationMatrix(srcArr.shape(), 
                                                                      trgArr.shape(), 
                                                                      src_element_size_um, 
                                                                      trg_element_size_um, 
                                                                      levA, 
                                                                      rowA, 
                                                                      colA);
  // Transform array
  transformArray(srcArr, invMat, trgArr);

  // Compute maximum intencity projection
  blitz::Array<unsigned char, 2> image = maxIntProj(trgArr);

  return image;
}


void savePPMImage( const blitz::Array<blitz::TinyVector<unsigned char,3>,2>& image,
                   const std::string& fileName)
{

  std::ofstream outFile( fileName.c_str(), std::ofstream::binary);
  
  outFile << "P6\n"
          << image.extent(1) << " " << image.extent(0) << " 255\n";

  outFile.write( reinterpret_cast<const char*>(image.dataFirst()), 
                 image.size() * sizeof( blitz::TinyVector<unsigned char,3>));

}

