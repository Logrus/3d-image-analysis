#include <iostream>
#include <fstream>
#include <sstream>
#include <iomanip>
#include <blitz/array.h>
#include <string>

// 1) Reimplementation of the mat-mat and mat-vec products
blitz::TinyMatrix<float, 4, 4> myproduct (blitz::TinyMatrix<float, 4, 4> a, blitz::TinyMatrix<float, 4, 4> b);
blitz::TinyVector<float, 4> myproduct (blitz::TinyMatrix<float, 4, 4> m, blitz::TinyVector<float, 4> v);

// 2) Nearest Neighbout interpolation
unsigned char interpolNN (const blitz::Array<unsigned char, 3> &arr, blitz::TinyVector<float, 4> pos);

// 3) Inverse transformation
void transformArray (const blitz::Array<unsigned char, 3> &srcArr, 
		     const blitz::TinyMatrix<float, 4, 4> &invMat,
		     blitz::Array<unsigned char, 3>  &trgArr);

// 4) Compute inverse matrix
blitz::TinyMatrix<float, 4, 4> createInverseRotationMatrix (const blitz::TinyVector<size_t, 3> &srcArrShape,
				const blitz::TinyVector<size_t, 3> &trgArrShape,
				const blitz::TinyVector<float, 3> &src_element_size_um,
				const blitz::TinyVector<float, 3> &trg_element_size_um,
				float angleAroundLev,
				float angleAroundRow,
				float angleAroundCol);

// 5) Save file function
void savePGMImage (const blitz::Array<unsigned char, 2> &Image, const std::string &fileName);


int main (int argc, char** argv)
{
 
  // Load data
  const char* filename = "Artemisia_pollen_71x136x136_8bit.raw";
  //const char* filename = "";
  int nLevels = 71;
  int nRows = 136;
  int nCols = 136;
  
  // Depending on the axis around which we rotate
  // x = sqrt (c^2 + l^2) around r
  // x = sqrt (r^2 + l^2) around c  
  // For pollen
  int x = ceil ( sqrt ( nCols*nCols + nLevels*nLevels ));
  // Create target array
  blitz::Array<unsigned char, 3> srcArr(nLevels, nRows, nCols);
  //std::cout << "x: " << x << std::endl;
  blitz::Array<unsigned char, 3> trgArr(x, nRows, x);
  blitz::TinyMatrix<float, 4, 4> invMat;

  std::ifstream file (filename, std::ifstream::in | std::ifstream::binary );
  if (file)
  {
    file.seekg (0, file.end);
    int length = file.tellg();
    file.seekg (0, file.beg);

    file.read (reinterpret_cast<char*>(srcArr.data()), length);
    
  } 
  blitz::TinyVector<float, 3> src_element_size_um; 
  blitz::TinyVector<float, 3> trg_element_size_um; 
  src_element_size_um = 0.4, 0.2, 0.2;
  trg_element_size_um = 0.4, 0.2, 0.2;
 for (int angle = 0; angle < 355; angle += 5){ 
  std::cout << "Angle= " << angle << std::endl;
  // Record images with rotations
  invMat = createInverseRotationMatrix(srcArr.shape(), trgArr.shape(), src_element_size_um, trg_element_size_um, 0, (3.14*angle)/180, 0);
  transformArray(srcArr, invMat, trgArr);

  // Compute maximum intencity projection
  blitz::Array<unsigned char, 2> image(nRows,x);
  image = 0;

  for (int lev = 0; lev < x; lev++)
    for (int row = 0; row < nRows; row++)
      for (int col = 0; col < x; col++){
  	if (image(row,col) < trgArr(lev, row, col)){
  	  image(row, col) = trgArr(lev, row, col); 
 	}
     }
  // Save output
  std::ostringstream filename;
  filename << "test_" << std::setw(3) << std::setfill('0') << angle << ".pgm";
  savePGMImage(image, filename.str());

 }

}


blitz::TinyMatrix<float, 4, 4> myproduct (blitz::TinyMatrix<float, 4, 4> a, blitz::TinyMatrix<float, 4, 4> b)
{
  blitz::TinyMatrix<float, 4, 4> result;
  float sum (0); 
  for (int lev = 0; lev < a.extent (0); ++lev) {
    for (int row = 0; row < a.extent (0); ++row) {
      for (int col = 0; col < a.extent (1); ++col) {
	sum = sum + a (lev, col) * b (col, row);
      }
      result (lev,row) = sum;
      sum = 0;
    }
  }
  return result;
}


blitz::TinyVector<float, 4> myproduct (blitz::TinyMatrix<float, 4, 4> m, blitz::TinyVector<float, 4> v)
{
  blitz::TinyVector<float, 4> result;

  float sum (0);

  for (int row = 0; row < m.extent (0); ++row) {
    for (int col = 0; col < m.extent (1); ++col) {
      sum = sum + m (row, col) * v (col);
    }
    result (row) = sum;
    sum = 0;
  }

  return result;
}

unsigned char interpolNN (const blitz::Array<unsigned char, 3> &arr, blitz::TinyVector<float, 4> pos)
{
  blitz::TinyVector<float, 4> new_pos = blitz::floor( pos + 0.5f ); 
  if ( blitz::all( new_pos >= 0 ) && blitz::all( new_pos < arr.shape() ) )
    return arr ( new_pos );
  else
    return 0;
}

void transformArray (const blitz::Array<unsigned char, 3> &srcArr, 
		     const blitz::TinyMatrix<float, 4, 4> &invMat,
		     blitz::Array<unsigned char, 3>  &trgArr)
{
  //std::cout << trgArr.extent(2) << std::endl;
  blitz::TinyVector<float, 4> trgPos, srcPos;
  for (int lev = 0; lev < trgArr.extent(0); ++lev){ 
    for (int row = 0; row < trgArr.extent(1); ++row){ 
      for (int col = 0; col < trgArr.extent(2); ++col){ 

	trgPos = lev, row, col, 1;
	srcPos = myproduct (invMat, trgPos);
	trgArr (lev, row, col) = interpolNN (srcArr, srcPos);

      }
    }
  }

}

blitz::TinyMatrix<float, 4, 4> createInverseRotationMatrix (const blitz::TinyVector<size_t, 3> &srcArrShape,
				const blitz::TinyVector<size_t, 3> &trgArrShape,
				const blitz::TinyVector<float, 3> &src_element_size_um,
				const blitz::TinyVector<float, 3> &trg_element_size_um,
				float angleAroundLev,
				float angleAroundRow,
				float angleAroundCol)
{
  blitz::TinyMatrix<float, 4, 4> result;
  
  // Definition of the seven matrixes
  blitz::TinyMatrix<float, 4, 4> shiftTrgCenterToOrigin, scaleToMicrometer, rotateAroundLev, rotateAroundRow, rotateAroundCol;
  blitz::TinyMatrix<float, 4, 4> scaleToVoxel, shiftOriginToScrCenter;
  
  shiftTrgCenterToOrigin = 1, 0, 0, -float(trgArrShape(0))/2,
			   0, 1, 0, -float(trgArrShape(1))/2,
			   0, 0, 1, -float(trgArrShape(2))/2,
			   0, 0, 0, 1;

  float sx = src_element_size_um(0);
  float sy = src_element_size_um(1);
  float sz = src_element_size_um(2);
  scaleToMicrometer = sx,  0,  0, 0,
		       0, sy,  0, 0,
		       0,  0, sz, 0,
		       0,  0,  0, 1;

  float a = angleAroundCol;
  rotateAroundCol = cos(a), -sin(a), 0, 0,
		    sin(a),  cos(a), 0, 0,
		         0,       0, 1, 0,
		         0,       0, 0, 1;

  a = angleAroundRow;
  rotateAroundRow =   cos(a),   0,  sin(a), 0,
		           0,   1,       0, 0,
		     -sin(a),   0,  cos(a), 0,
		           0,   0,       0, 1;

  a = angleAroundLev;
  rotateAroundLev = 1,      0,       0, 0, 
                    0, cos(a), -sin(a), 0,
		    0, sin(a),  cos(a), 0,
		    0,      0,       0, 1;

  sx = 1.0/trg_element_size_um(0);
  sy = 1.0/trg_element_size_um(1);
  sz = 1.0/trg_element_size_um(2);
  scaleToVoxel = sx,  0,  0, 0,
		  0, sy,  0, 0,
		  0,  0, sz, 0,
		  0,  0,  0, 1;

  shiftOriginToScrCenter = 1, 0, 0, float(srcArrShape(0))/2,
			   0, 1, 0, float(srcArrShape(1))/2,
			   0, 0, 1, float(srcArrShape(2))/2,
			   0, 0, 0, 1;

  result = myproduct(scaleToMicrometer, shiftTrgCenterToOrigin);
  result = myproduct(rotateAroundCol, result);
  result = myproduct(rotateAroundRow, result);
  result = myproduct(rotateAroundLev, result);
  result = myproduct(scaleToVoxel, result);
  result = myproduct(shiftOriginToScrCenter, result);
  
  return result;
}


void savePGMImage (const blitz::Array<unsigned char, 2> &Image, const std::string &fileName)
{
  std::ofstream outf (fileName.c_str());
  outf << "P5\n" << Image.extent(1) << " " << Image.extent(0) << "\n255\n";
  outf.write (reinterpret_cast<const char*>(Image.dataFirst()), Image.size()*sizeof(char));
  outf.close();
}


