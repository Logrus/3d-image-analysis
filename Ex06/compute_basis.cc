// ===============================
//
// 3D IMAGE ANALYSIS
//
// Exercise 06. 
// Vladislav Tananaev
// tananaev@cs.uni-freiburg.de
// v.d.tananaev@gmail.com
//
// ===============================
#define BZ_DEBUG
#include <iostream>
#include <gsl/gsl_sf_legendre.h>
#include <blitz/array.h>
#include <complex>
#include <math.h>
#include "BlitzHDF5Helper.hh"
using std::cout;
using std::endl;
using std::complex;

// This function computes the basis functions of the spherical 
// harmonics on a grid 360x180
int main(){

  int max_band = 6;
  int step = 0;

  blitz::Array<double, 3> basis_real(49, 180, 360);
  blitz::Array<double, 3> basis_imag(49, 180, 360);

  for (int l=0; l<=max_band; l++){
    for (int m=0; m<=l; m++){

      for (int lng=0; lng<180; lng++)
        for (int lat=0; lat<360; lat++){

          // P(cos theta) exp ( i phi m )
          // phi/lat in [0, 2pi]
          // theta/lng in [0, pi]
          double cos_val = cos(lng*M_PI/180);
          complex<double> exp_val = exp( complex<double>(0, m*lat*2*M_PI/360) );
          complex<double> ans =  gsl_sf_legendre_sphPlm( l, m, cos_val)*exp_val;
          // Positive part
          basis_real(step+l+m, lng, lat) = ans.real();
          basis_imag(step+l+m, lng, lat) = ans.imag();
          // Negative part
          int neg = m % 2 ? -1 : 1;
          basis_real(step+l-m, lng, lat) = ans.real()*neg;
          basis_imag(step+l-m, lng, lat) = -ans.imag()*neg;
          
      }

    }
    step = step + (2*l+1);
  }


  std::string outFileName="out.h5";
  remove ( outFileName.c_str() );
  writeBlitzToHDF5 (basis_real, "real", outFileName);
  writeBlitzToHDF5 (basis_imag, "imag", outFileName);

  // Compute orthogonality
  blitz::Array<double, 2> ort_real(49,49);
  blitz::Array<double, 2> ort_imag(49,49);
  double lng_step = M_PI/180;
  double lat_step = 2*M_PI/360;
  for (int i=0;i<49;i++)
  for (int j=0;j<49;j++){

    complex<double> sum(0., 0.);

    for (int lng=0; lng<180; lng++)
    for (int lat=0; lat<360; lat++){

      
      complex<double> c1( basis_real(i, lng, lat ),  basis_imag(i, lng, lat ) );
      complex<double> c2( basis_real(j, lng, lat ), -basis_imag(j, lng, lat ) );
      sum += c1*c2 * sin(lng * lng_step)*lng_step*lat_step;
    }

    if (sum.real() < 1./1000) sum.real(0);
    if (sum.imag() < 1./1000) sum.imag(0);
    
    // For nice output below round values that more than 0.9
    // if (sum.real() > 0.9) sum.real(1);
    ort_real(i,j) = sum.real();
    ort_imag(i,j) = sum.imag();
    
  }

  cout << "Check orthogonality: " << endl;
  cout << ort_real << endl;
  outFileName="orthogonality.h5";
  remove ( outFileName.c_str() );
  writeBlitzToHDF5 (ort_real, "real", outFileName);
  writeBlitzToHDF5 (ort_imag, "imag", outFileName);

  return 0;


}
