// ===============================
//
// 3D IMAGE ANALYSIS
//
// Exercise 06. 
// Vladislav Tananaev
// tananaev@cs.uni-freiburg.de
// v.d.tananaev@gmail.com
//
// ===============================
#define BZ_DEBUG
#include <iostream>
#include <gsl/gsl_sf_legendre.h>
#include <blitz/array.h>
#include <complex>
#include <math.h>
#include <vector>
#include "BlitzHDF5Helper.hh"
using std::cout;
using std::endl;
using std::complex;
using std::vector;

// This function computes the basis functions of the spherical 
// harmonics on a grid 360x180
int main(){

  double lng_step = M_PI/180;
  double lat_step = 2*M_PI/360;
  // Precompute sin/cos and all constants
  vector<double> cos_table (180);
  vector<double> sin_table (180);
  for (int i=0; i< 180; ++i){
    cos_table[i] = cos(i*lng_step);
    sin_table[i] = sin(i*lng_step)*lng_step*lat_step;
  }

  int max_band = 10;
  int size_band = (max_band+1)*(max_band+1);
  int step = 0;

  blitz::Array<double, 3> result(max_band+1, 180, 360);
  blitz::Array<double, 2> elevation(180, 360);

  // Read world evaluation h5 file
  std::string inFileName="world_elevation.h5";
  readHDF5toBlitz (inFileName, "elevation", elevation);

  blitz::Array<double, 3> basis_real(size_band, 180, 360);
  blitz::Array<double, 3> basis_imag(size_band, 180, 360);

  cout << "Started computing basis functions..." <<endl;
  for (int l=0; l<=max_band; l++){
  for (int m=0; m<=l; m++){

    for (int lng=0; lng<180; lng++)
      for (int lat=0; lat<360; lat++){

        //double cos_val = cos(lng*M_PI/180);
        complex<double> exp_val = exp( complex<double>(0, m*lat*2*M_PI/360) );
        complex<double> ans =  gsl_sf_legendre_sphPlm( l, m, cos_table[lng])*exp_val;
        // Positive part
        basis_real(step+l+m, lng, lat) = ans.real();
        basis_imag(step+l+m, lng, lat) = ans.imag();
        // Negative part
        int neg = m % 2 ? -1 : 1;
        basis_real(step+l-m, lng, lat) = ans.real()*neg;
        basis_imag(step+l-m, lng, lat) = -ans.imag()*neg;
        
      }

    }
  step = step + (2*l+1);
  }

  cout << "Computed basis functions!" << endl << "Computing coefficients..." << endl;
  // Compute spherical harmonics up-to 30 band
  blitz::Array<double, 1> c_r(size_band);
  blitz::Array<double, 1> c_i(size_band);
  for (int i=0;i<size_band;i++){

    complex<double> sum(0., 0.);

    for (int lng=0; lng<180; lng++)
    for (int lat=0; lat<360; lat++){

      
      complex<double> f( elevation(lng, lat ),  0 );
      complex<double> Y( basis_real(i, lng, lat ), -basis_imag(i, lng, lat ) );
      sum += f*Y * sin_table[lng];

    }

    c_r(i) = sum.real();
    c_i(i) = sum.imag();
    
  }

  cout << "Computed coefficients!" << endl << "Reconstructing the world..." << endl;
  result = 0;
  step = 0;
  // Reconstruct the world with 1, 2, 3 ... band
  for (int band=0; band<=max_band; band++){

    cout << "Band: " << band << endl;
    if( band != 0 )
      result(band, blitz::Range::all(), blitz::Range::all() ) = result(band-1, blitz::Range::all(), blitz::Range::all() );

    for(int i=step; i <= step+2*band; i++){

    complex<double> c( c_r(i),  c_i(i) );

    for (int lng=0; lng<180; lng++)
        for (int lat=0; lat<360; lat++){
          
          complex<double> Y( basis_real(i, lng, lat ), basis_imag(i, lng, lat ) );
          complex<double> res =  c*Y * sin_table[lng];
          result(band, lng, lat) += res.real();

        }

    }
    // cout << "step: " << step << endl;
    step = step + (2*band+1);

  }

  // Save result to HDF5
  std::string outFileName="reconstructed_world.h5";
  remove ( outFileName.c_str() );
  writeBlitzToHDF5 (result, "reconstruction", outFileName);

  return 0;
}
