#include <iostream>
#include <fstream>
#include <sstream>
using namespace std;

int main(){
  
  int nLevels = 124;
  int nRows = 216;
  int nCols = 181;

  const char* filename = "whatisit_124x216x181_8bit.raw";
  
  ifstream file( filename, ifstream::in | ifstream::binary );

  const char* header = "P5\n181 216\n255\n";

  if(file){
    file.seekg (0, file.end);
    int length = file.tellg();
    file.seekg (0, file.beg);

    unsigned char* buffer = new unsigned char[length];

    file.read(reinterpret_cast<char*>(buffer), length);

    if(file)
      cout << filename << " has successfully been read." << endl;
    else
      cout << "Couldn't read " << filename << endl;
    
    // XY slices
    for (int level = 0; level < nLevels; level++){

      stringstream lvl;
      lvl << level;
      string outfilename = "slice_xy_" + lvl.str() + ".pgm";
      ofstream outf( outfilename.c_str() ); 

      int out_len = nRows*nCols;
      outf.write (header, 15*sizeof(char) );
      outf.write (level*out_len + reinterpret_cast<char*>(buffer), out_len);

      outf.close();

    }
   
 
    // XZ slices
    int out_len = nLevels*nCols;
    unsigned char * out_buf = new unsigned char[out_len];
    header = "P5\n181 124\n255\n";

    for (int row = 0; row < nRows; row++){

          stringstream lvl;
          lvl << row;
          string outfilename = "slice_xz_" + lvl.str() + ".pgm";
          ofstream outf( outfilename.c_str() ); 

          for ( int col = 0; col < nCols; col++)
            for ( int level =0; level < nLevels; level++){
              out_buf[nCols*level + col] = buffer[level*nRows*nCols + row*nCols + col];
            }
          outf.write (header, 15*sizeof(char) );
          outf.write (reinterpret_cast<char*>(out_buf), out_len);

          outf.close();

    }
    delete[] out_buf; 
    
    // YZ slices
    out_len = nLevels*nRows;
    out_buf = new unsigned char[out_len];
    header = "P5\n124 216\n255\n";

    for (int col = 0; col < nCols; col++){

          stringstream lvl;
          lvl << col;
          string outfilename = "slice_yz_" + lvl.str() + ".pgm";
          ofstream outf( outfilename.c_str() ); 

          for ( int row = 0; row < nRows; row++)
            for ( int level =0; level < nLevels; level++){
              out_buf[nLevels*row + level] = buffer[level*nRows*nCols + row*nCols + col];
            }
          outf.write (header, 15*sizeof(char) );
          outf.write (reinterpret_cast<char*>(out_buf), out_len);

          outf.close();

    }
    delete[] out_buf; 

    file.close();
    delete[] buffer;
  } 

  return 0;
}

