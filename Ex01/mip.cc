#include <iostream>
#include <fstream>
using namespace std;

int main(){
  
  int nLevels = 124;
  int nRows = 216;
  int nCols = 181;

  const char* filename = "whatisit_124x216x181_8bit.raw";
  
  ifstream file( filename, ifstream::in | ifstream::binary );

  const char* header = "P5\n181 216\n255\n";

  if(file){
    file.seekg (0, file.end);
    int length = file.tellg();
    file.seekg (0, file.beg);

    unsigned char* buffer = new unsigned char[length];

    file.read(reinterpret_cast<char*>(buffer), length);

    if(file)
      cout << filename << " has successfully been read." << endl;
    else
      cout << "Couldn't read " << filename << endl;
    
    ofstream outf( "mip.pgm" ); 

    int out_len = nRows*nCols;
    unsigned char* mip = new unsigned char[out_len];
    
    fill(mip, mip+out_len, 0);

    for (int level = 0; level < nLevels; level++)
      for (int row = 0; row < nRows; row++)
        for (int col = 0; col < nCols; col++){
          if (mip[row*nCols + col] < buffer[level*nRows*nCols + row*nCols + col]){
            mip[row*nCols + col] = buffer[level*nRows*nCols + row*nCols + col];
          }
        }

    outf.write (header, 15*sizeof(char) );

    outf.write (reinterpret_cast<char*>(mip), out_len);

    outf.close();

   
 
    file.close();
    delete[] mip;
    delete[] buffer;
  } 

  return 0;
}

