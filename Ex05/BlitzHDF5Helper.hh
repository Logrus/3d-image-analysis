/* @brief Simple library to read and write HDF5 from and to Blitz
 *
 * Version 0.2
 * 
 * The header file provides two functions:
 *
 *   readHDF5toBlitz
 *
 *   writeBlitzToHDF5
 *
 *   readBlitzTinyVectorFromHDF5Attribute
 *
 *   writeBlitzTinyVectorToHDF5Attribute
 *
 * Author : Ahmed Abdulkadir
 * Date   : 18.06.2013
 *
 * Data type must be float or double.
 *
 * @note This was tested with HDF5 1.8.11 on Mac OSX 10.8.3 and Ubuntu 12.04 x86_64.
 *       The functions are experimental. They may or may not do what you expect.
 * 
 * History :
 *	2014-06-20 (Robert Bensch): Added HDF5 compile-time version flags defines. Necessary in case HDF5 function macros map to wrong version-numbered functions.
 *	2014-06-20 (Robert Bensch): Shifted helper functions definitions to the top of the header file. GCC versions >= 4.7 are more restrictive and complain otherwise.
 * 
 */

// HDF5 compile-time version flags:
#define H5Dopen_vers 2
#define H5Dcreate_vers 2
#define H5Acreate_vers 2

// general includes
#include <string>
#include <iostream>
#include <fstream>
// HDF5 includes
#include <hdf5.h>
// Blitz++
#include <blitz/array.h>


/** Below are some dummy functions that allow somehow automatic
    handling of data types */
template<typename DTYPE, int RANK>
int getBlitzArrayRank(const blitz::Array<DTYPE, RANK>&) {
     return RANK;
}

template<typename DTYPE, int RANK, int DIM>
int getBlitzArrayRank(const blitz::Array<blitz::TinyVector<DTYPE,DIM>, RANK>&) {
     return RANK + 1;
}

template<typename DTYPE, int RANK>
int getBlitzArrayDim(const blitz::Array<DTYPE, RANK>&) {
     return 0;
}

template<typename DTYPE, int RANK, int DIM>
int getBlitzArrayDim(const blitz::Array<blitz::TinyVector<DTYPE,DIM>, RANK>&) {
     return DIM;
}

template<int RANK, int DIM>
hid_t h5DataType(const blitz::Array<blitz::TinyVector<float,DIM>, RANK>&) {
     return H5T_NATIVE_FLOAT;
}

template<int RANK, int DIM>
hid_t h5DataType(const blitz::Array<blitz::TinyVector<char,DIM>, RANK>&) {
     return H5T_NATIVE_FLOAT;
}

template<int RANK, int DIM>
hid_t h5DataType(const blitz::Array<blitz::TinyVector<int,DIM>, RANK>&) {
     return H5T_NATIVE_FLOAT;
}

template<int RANK, int DIM>
hid_t h5DataType(const blitz::Array<blitz::TinyVector<double,DIM>, RANK>&) {
     return H5T_NATIVE_DOUBLE;
}

template<int RANK>
hid_t h5DataType(const blitz::Array<float,RANK>&) {
     return H5T_NATIVE_FLOAT;
}

template<int RANK>
hid_t h5DataType(const blitz::Array<double,RANK>&) {
     return H5T_NATIVE_DOUBLE;
}

template<int RANK>
hid_t h5DataType(const blitz::TinyVector<float,RANK>&) {
     return H5T_NATIVE_FLOAT;
}

template<int RANK>
hid_t h5DataType(const blitz::TinyVector<double,RANK>&) {
     return H5T_NATIVE_DOUBLE;
}


/** @brief Write Blitz++ data array to HDF5 file
 *
 *  Target data type is determined from output array type. Automatic type conversion
 *  is attempted. The rank of the HDF5 data set and that of the Blitz++ array
 *  must match.
 *write
 *  @return      status               exit status
 *
 *  @param[in]  sourceBlitzArray     Blitz++ array containing source data
 *  @param[in]  targetDataSetName    name of the target data set
 *  @param[in]  targetFileName       file name of the HDF5 file to be written
 *
 *  @note Not all data types, storage, byte-orders and endiannesses are sopported.
 *
 */
template<typename DTYPE, int RANK>
int writeBlitzToHDF5(const blitz::Array<DTYPE, RANK> &sourceBlitzArray,
                      const std::string targetDataSetName,
                      const std::string targetFileName) {

    /** exit status */
    int status = 0;

    // rank of Blitz++ array
    int         sourceBlitzArrayRank = getBlitzArrayRank(sourceBlitzArray);

    // rank of tensor data type in Blitz++ array (i.e. number of dimensions)
    int         sourceBlitzArrayDim  = getBlitzArrayDim(sourceBlitzArray);

    /** prompt information about the source blitz::Array */
    std::cout << "\nInformation about the structure of the source blitz::Array:"
	      << std::endl;
    sourceBlitzArray.dumpStructureInformation(std::cout);

    /** HDF5 data space */
    hid_t       dataSpaceID;

    /** HDF5 data set */
    hid_t       dataSet;

    // shape of array
    hsize_t     sourceShape[sourceBlitzArrayRank];
    hsize_t     maxShape[sourceBlitzArrayRank];
    for (int d = 0; d < RANK; ++d) {
	sourceShape[d] = sourceBlitzArray.shape()[d];
	maxShape[d] = sourceBlitzArray.shape()[d];
    }
    if (sourceBlitzArrayRank == (RANK+1)) {
	sourceShape[RANK] = sourceBlitzArrayDim;
	maxShape[RANK]    = sourceBlitzArrayDim;
    std::cout << "* sourceBlitzArrayRank: " << sourceBlitzArrayRank 
	      << " " << sourceBlitzArray.rank() 
	      << " " << RANK 
	      << " " << sourceBlitzArrayDim << std::endl;
    }



    /** Error status of HDF5 function */
    herr_t      errorStatus = 0;

    // create output file and corresponding dataset (data type must be known in advance!)
    hid_t       targetFileHandle;
    htri_t      isHDF5;

    std::ifstream ifile(targetFileName.c_str());
    if (ifile) {
	isHDF5 = H5Fis_hdf5(targetFileName.c_str());
    } else {
	isHDF5 = -1;
    }


    if (isHDF5 < 0) {
	if(( targetFileHandle = H5Fcreate(targetFileName.c_str(),
					H5F_ACC_TRUNC, H5P_DEFAULT, H5P_DEFAULT)) < 0 ) {
	std::cerr << "Error: fail to create file '" << targetFileName
		  << "'.\n " << std::endl;
	return targetFileHandle;
	}
    } else if (isHDF5 > 0) {
	if(( targetFileHandle = H5Fopen(targetFileName.c_str(),H5F_ACC_RDWR, H5P_DEFAULT)) < 0) {
	    std::cerr << "Error: faild to open file '" << targetFileName
		      << "'.\n " << std::endl;
	    return targetFileHandle;
	}
    } else if (isHDF5 == 0) {
	std::cerr << "Error: file '" << targetFileName << "' exists, but is not of type HDF5."
		  << "'.\n " << std::endl;
	    return -1;

    }

    ifile.close();

    /** Initialize simple data set
    *
    *  HDF5 uses C storage conventions, assuming that the index of last listed dimension
    *  grows fastest and the index of the first listed dimension grows slowest.
    *
    *  @sa http://www.hdfgroup.org/HDF5/doc/UG/UG_frame12Dataspaces.html
    */
    dataSpaceID = H5Screate(H5S_SIMPLE);
    if (dataSpaceID < 0) {
	std::cerr << "Error: execution of 'H5Screate(H5S_SIMPLE)' failed. " << std::endl;
    }

    /** Set simple data space extents */
    errorStatus = H5Sset_extent_simple(dataSpaceID,sourceBlitzArrayRank,sourceShape,maxShape);
    if (errorStatus < 0) {
	std::cerr << "Error: H5Sset_extent_simple() failed. " << std::endl;
	return errorStatus;
    }

    /** determine source data type */
    hid_t sourceDataSetNativeDataType = h5DataType(sourceBlitzArray);

    /** detemine if requested data set exists exists */
    bool datasetExists = H5Lexists(targetFileHandle, targetDataSetName.c_str(), H5P_DEFAULT);

    /** Create or open data set */
    if (datasetExists) {
	dataSet = H5Dopen(targetFileHandle, targetDataSetName.c_str(),H5P_DEFAULT);
    } else {
	dataSet     = H5Dcreate(targetFileHandle, targetDataSetName.c_str(),
				sourceDataSetNativeDataType, dataSpaceID,
				H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
    };


    /** check compatibility of source and target data types */
    //sourceBlitzArray.T_numtype; // float, double, char, int

    /*
    * Write the data to the dataset using default transfer properties.
    */
    errorStatus = H5Dwrite(dataSet, sourceDataSetNativeDataType, H5S_ALL, H5S_ALL,
			  H5P_DEFAULT, sourceBlitzArray.dataFirst() );
    if (errorStatus < 0) {
	std::cerr << "Error: H5Dwrite() failed. " << std::endl;
	return errorStatus;
    }

    /*
    * Close/release resources.
    */
    errorStatus = H5Sclose(dataSpaceID);
    if (errorStatus < 0) {
	std::cerr << "Error while closing data space. " << std::endl;
    }
    errorStatus = H5Dclose(dataSet);
    if (errorStatus < 0) {
	std::cerr << "Error while closing data set. " << std::endl;
    }
    errorStatus = H5Fclose(targetFileHandle);
    if (errorStatus < 0) {
	std::cerr << "Error while closing file handle. " << std::endl;
	return errorStatus;
    }

    return status;
}


/* @brief Read data array from HDF5 file and store it in Blitz++ array
 *
 * Target data type is determined from output array type. Automatic type conversion
 * is attempted. The rank of the HDF5 data set and that of the Blitz++ array
 * must match.
 *
 *  @return     status               exit status
 *
 *  @param[in]  sourceFileName       file name of the HDF5 file to be read 
 *  @param[in]  sourceDataSetName    name of the source data set
 *  @param[out] targetBlitzArray     target Blitz++ array
 *
 *  @note       Not all data types, storage, byte-orders and endiannesses are sopported.
 *
 */
template<class DTYPE, int RANK>
int readHDF5toBlitz(const std::string sourceFileName, const std::string sourceDataSetName,
                     blitz::Array<DTYPE, RANK> &targetBlitzArray) {

    /** exit status */
    int status = 0;

    // open the source file
    hid_t        sourceFileHandle;
    if ((sourceFileHandle  = H5Fopen(sourceFileName.c_str(), H5F_ACC_RDONLY, H5P_DEFAULT)) < 0) {
	std::cerr << "Error: fail to open file '" << sourceFileName
		  << "'.\n " << std::endl;
	return 1;
    };

    // hdf5 error status
    herr_t       errorStatus;
    // rank of the data (i.e. number of dimensions)
    int          sourceRank;

    /** HDF5 data set */
    hid_t       dataSet = H5Dopen(sourceFileHandle, sourceDataSetName.c_str(), H5P_DEFAULT);

    /** HDF5 data space */
    hid_t       dataSpaceID = H5Dget_space(dataSet);;

    // rank of Blitz++ array
    int         targetBlitzArrayRank = getBlitzArrayRank(targetBlitzArray);

    // rank of tensor data type in Blitz++ array
    int         targetBlitzArrayDim  = getBlitzArrayDim(targetBlitzArray);

    // read rank of input image
    sourceRank = H5Sget_simple_extent_ndims(dataSpaceID);

    // shape of array
    hsize_t     sourceShape[sourceRank];
    //hsize_t     maxShape[sourceRank];

    // read dimensionality, type and size of input data
    errorStatus  = H5Sget_simple_extent_dims(dataSpaceID, sourceShape, NULL);

    // data type
    hid_t       sourceDataType  = H5Dget_type(dataSet);

    // order
    H5T_order_t sourceDataByteOrder  = H5Tget_order(sourceDataType);

    // data type of input
    H5T_class_t sourceDataClass = H5Tget_class(sourceDataType);

    // data set size in bytes
    size_t      sourceDataTypeSize = H5Tget_size(sourceDataType);

    // data set size, i.e. number of elements in the data set
    size_t      sourceNumberOfElements = 1;
    for (int d=0; d<sourceRank; ++d) sourceNumberOfElements *= sourceShape[d];

    // print dataset characteristic
    std::cout << "\nDataset characteristic of " << sourceFileName << " -> /"
	      << sourceDataSetName << std::endl
	      << " dataSetClass  : " << sourceDataClass    << std::endl
	      << " dataSetType   : " << sourceDataType     << std::endl
	      << " rank          : " << sourceRank            << std::endl
	      << " shape         : ";
    for (int d=0; d<sourceRank; ++d) std::cout << sourceShape[d] << " ";
    std::cout << std::endl
	      << " dataTypeSize  : " << sourceDataTypeSize << std::endl
	      << " dataSetSize   : " << sourceNumberOfElements << std::endl
	      << " byte-order (LE/BE) : " << sourceDataByteOrder << std::endl << std::endl;

    // continue only if rank of source HDF5 and target
    if ( (sourceRank) != (targetBlitzArrayRank) ) {
	std::cerr << "Error: ranks are not compatible!" << std::endl
		  << "       source rank = " << sourceRank
		  << ", targen rank = " << (targetBlitzArrayRank )
		  << std::endl;;
	return 1;
    }

    /** check if data is compatible */
    if (targetBlitzArrayDim > 0) {
	if ((int)sourceShape[sourceRank-1] != targetBlitzArrayDim) {
	      std::cerr << "Error: dimensions are incompatible!" << std::endl;
	      return 1;
	}
    }

    /** check if data types are compatible */
    hid_t sourceDataSetNativeDataType = H5Tget_native_type(sourceDataType, H5T_DIR_ASCEND);

    /*      H5T_NATIVE_CHAR         
	    H5T_NATIVE_SHORT        
	    H5T_NATIVE_INT          
	    H5T_NATIVE_LONG         
	    H5T_NATIVE_LLONG        

	    H5T_NATIVE_UCHAR
	    H5T_NATIVE_USHORT
	    H5T_NATIVE_UINT
	    H5T_NATIVE_ULONG
	    H5T_NATIVE_ULLONG */

    /*
    case H5T_NATIVE_CHAR:
    case H5T_NATIVE_SHORT:
    case H5T_NATIVE_INT:
    case H5T_NATIVE_LONG:
    case H5T_NATIVE_LLONG:

    case H5T_NATIVE_UCHAR:
    case H5T_NATIVE_USHORT:
    case H5T_NATIVE_UINT:
    case H5T_NATIVE_ULONG:
    case H5T_NATIVE_ULLONG:

    case H5T_NATIVE_FLOAT:
    case H5T_NATIVE_DOUBLE:
    case H5T_NATIVE_LDOUBLE:
    } */


    // resize Blitz++ array to match HDF5 Array
    blitz::TinyVector<int, RANK> arrayShape;
    for (int d=0; d<RANK; ++d) {
	arrayShape[d] = sourceShape[d];
    }

    targetBlitzArray.resize(arrayShape);

    /** Create data set */
    dataSet     = H5Dopen(sourceFileHandle, sourceDataSetName.c_str(), H5P_DEFAULT);
    if (dataSet < 0) {
	std::cerr << "Error: H5Dopen failed. " << std::endl;
	return errorStatus;
    }

    /** Data Space ID */
    dataSpaceID = H5Dget_space(dataSet);
    if (dataSpaceID < 0) {
	std::cerr << "Error: H5Dget_space() failed. " << std::endl;
	return errorStatus;
    }

    errorStatus = H5Dread(dataSet, sourceDataSetNativeDataType, dataSpaceID,
			  dataSpaceID, H5P_DEFAULT,
			  targetBlitzArray.dataFirst());
    if (errorStatus < 0) {
	std::cerr << "Error: H5Dread() failed. " << std::endl;
	return errorStatus;
    }

    /*
    * Close/release resources.
    */

    errorStatus = H5Sclose(dataSpaceID);
    if (errorStatus < 0) {
	std::cerr << "Error while closing data space. " << std::endl;
    }

    errorStatus = H5Dclose(dataSet);
    if (errorStatus < 0) {
	std::cerr << "Error while closing data set. " << std::endl;
    }
    errorStatus = H5Fclose(sourceFileHandle);
    if (errorStatus < 0) {
	std::cerr << "Error while closing file handle. " << std::endl;
	return errorStatus;
    }


    return status;   
}

/* @brief Write data from Blitz++ TinyVector to HDF5 file
 *
 *  @return     status               exit status, non-zero if error occurs
 *
 *  @param[in]  attributeData        attribute data
 *  @param[in]  targetDataSetName    data set to bind attribute to
 *  @param[in]  targetFilename       name of HDF5 file
 *
 *
 */
template<class DTYPE, int DIMS>
int writeBlitzTinyVectorToHDF5Attribute(const blitz::TinyVector<DTYPE, DIMS> &attributeData,
                                        const std::string attributeName,
                                        const std::string targetDataSetName,
                                        const std::string targetFileName) {
     herr_t errorStatus = 0;

     hsize_t attributeDimensions = DIMS;

     /** create handle to target file */
     hid_t      targetFileHandle  = H5Fopen(targetFileName.c_str(), H5F_ACC_RDWR, 
                                              H5P_DEFAULT);
    if (targetFileHandle < 0) {
         std::cerr << "Error: Failed to open " << targetFileName << " using 'H5Fopen'." 
                    << std::endl;
          return errorStatus;
     }

     hid_t      targetDataSet = H5Dopen(targetFileHandle, targetDataSetName.c_str(),
                                        H5P_DEFAULT);

     if (targetDataSet < 0) {
          std::cerr << "Error: 'H5Dopen' failed."
                    << std::endl;
          return errorStatus;
     }

     hid_t      attributeDataSpace = H5Screate(H5S_SIMPLE);
     errorStatus          = H5Sset_extent_simple(attributeDataSpace, 1,
                                                 &attributeDimensions, NULL);
     if (errorStatus < 0) {
          std::cerr << "Error: Definition of data space extent failed (H5Sset_extent_simple)." 
                    << std::endl;
          return errorStatus;
     }

     htri_t attributeExist = H5Aexists( targetDataSet, attributeName.c_str() );

     if (attributeExist > 0) {
          errorStatus = H5Adelete(targetDataSet, attributeName.c_str());
     }

     /** determine source data type */
     hid_t sourceDataSetNativeDataType = h5DataType(attributeData);

     hid_t attribute = H5Acreate(targetDataSet, attributeName.c_str(),
                                       sourceDataSetNativeDataType, attributeDataSpace, H5P_DEFAULT, 
                                       H5P_DEFAULT);

     if (attribute < 0) {
          std::cerr << "Error: could not create attribute."
                    << std::endl;
          return errorStatus;
     }

     errorStatus = H5Awrite(attribute, sourceDataSetNativeDataType, (void*)attributeData.data() );
     if (errorStatus < 0) {
          std::cerr << "Error: attribute write failed."
                    << std::endl;
          return errorStatus;
     }

     errorStatus = H5Aclose(attribute);

     return errorStatus;
}

/* @brief Read data from Blitz++ TinyVector to HDF5 file
 *
 *  @return     status               exit status, non-zero if error occurs
 *
 *  @param[out] attributeData        attribute data
 *  @param[in]  attributeName        name of attribute in HDF5 data set
 *  @param[in]  sourceDataSetName    data set where attribute is bound to
 *  @param[in]  sourceFilename       name of HDF5 file
 *
 */
template<class DTYPE, int DIMS>
int readBlitzTinyVectorFromHDF5Attribute(blitz::TinyVector<DTYPE, DIMS> &attributeData,
                                         const std::string attributeName,
                                         const std::string sourceDataSetName,
                                         const std::string sourceFileName) {
     herr_t errorStatus;

     hsize_t attributeDimensions = DIMS;

     /** create handle to file */
     hid_t      sourceFileHandle  = H5Fopen(sourceFileName.c_str(), H5F_ACC_RDONLY, H5P_DEFAULT);
     if (sourceFileHandle < 0) {
          std::cerr << "Error while opening file '" << sourceFileName << "'." << std::endl;
          return sourceFileHandle;
     }

     hid_t      sourceDataSet = H5Dopen(sourceFileHandle, sourceDataSetName.c_str(),
                                         H5P_DEFAULT);
     if (sourceDataSet < 0) {
          std::cerr << "Error while opening data set." << std::endl;
          return sourceDataSet;
     }

     hid_t      attributeDataSpace = H5Screate(H5S_SIMPLE);
     errorStatus          = H5Sset_extent_simple(attributeDataSpace, 1, 
                                                 &attributeDimensions, NULL);
     if (errorStatus < 0) {
          std::cerr << "Error: Definition of data space extent failed (H5Sset_extent_simple)." 
                    << std::endl;
          return errorStatus;
     }


     hid_t      attribute = H5Aopen_name(sourceDataSet, attributeName.c_str());
     if (attribute < 0) {
          std::cerr << "Error while opening attribute." << std::endl;
          return errorStatus;
     }

     /** determine source data type */
     hid_t sourceDataSetNativeDataType = h5DataType(attributeData);

     errorStatus = H5Aread(attribute, sourceDataSetNativeDataType, (void*)attributeData.data());
     if (errorStatus < 0) {
          std::cerr << "Error while reading attribute. " << std::endl;
          return errorStatus;
     }

     errorStatus = H5Aclose(attribute);
     if (errorStatus < 0) {
          std::cerr << "Error while closing attribute. " << std::endl;
          return errorStatus;
     }

     errorStatus = H5Sclose(attributeDataSpace);
     if (errorStatus < 0) {
          std::cerr << "Error while closing data space. " << std::endl;
          return errorStatus;
     }
     errorStatus = H5Dclose(sourceDataSet);
     if (errorStatus < 0) {
          std::cerr << "Error while closing data set. " << std::endl;
          return errorStatus;
     }
     errorStatus = H5Fclose(sourceFileHandle);
     if (errorStatus < 0) {
          std::cerr << "Error while closing file handle. " << std::endl;
          return errorStatus;
     }

     return errorStatus;
}
