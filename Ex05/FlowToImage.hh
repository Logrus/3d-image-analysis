/*
 * Visualization of optical flow direction and magnitude,
 * source code from Thomas Brox
 * 
 * Modified by Robert Bensch
 */

#include <math.h> 

// cartesianToRGB
void cartesianToRGB (float x, float y, float& R, float& G, float& B) {
  float radius = sqrt (x * x + y * y);
  if (radius > 1) radius = 1;
  float phi;
  if (x == 0.0)
    if (y >= 0.0) phi = 0.5 * M_PI;
    else phi = 1.5 * M_PI;
  else if (x > 0.0)
    if (y >= 0.0) phi = atan (y/x);
    else phi = 2.0 * M_PI + atan (y/x);
  else phi = M_PI + atan (y/x);
  float alpha, beta;    // weights for linear interpolation
  phi *= 0.5;
  // interpolation between red (0) and blue (0.25 * M_PI)
  if ((phi >= 0.0) && (phi < 0.125 * M_PI)) {
    beta  = phi / (0.125 * M_PI);
    alpha = 1.0 - beta;
    R = (int)(radius * (alpha * 255.0 + beta * 255.0));
    G = (int)(radius * (alpha *   0.0 + beta *   0.0));
    B = (int)(radius * (alpha *   0.0 + beta * 255.0));
  }
  if ((phi >= 0.125 * M_PI) && (phi < 0.25 * M_PI)) {
    beta  = (phi-0.125 * M_PI) / (0.125 * M_PI);
    alpha = 1.0 - beta;
    R = (int)(radius * (alpha * 255.0 + beta *  64.0));
    G = (int)(radius * (alpha *   0.0 + beta *  64.0));
    B = (int)(radius * (alpha * 255.0 + beta * 255.0));
  }
  // interpolation between blue (0.25 * M_PI) and green (0.5 * M_PI)
  if ((phi >= 0.25 * M_PI) && (phi < 0.375 * M_PI)) {
    beta  = (phi - 0.25 * M_PI) / (0.125 * M_PI);
    alpha = 1.0 - beta;
    R = (int)(radius * (alpha *  64.0 + beta *   0.0));
    G = (int)(radius * (alpha *  64.0 + beta * 255.0));
    B = (int)(radius * (alpha * 255.0 + beta * 255.0));
  }
  if ((phi >= 0.375 * M_PI) && (phi < 0.5 * M_PI)) {
    beta  = (phi - 0.375 * M_PI) / (0.125 * M_PI);
    alpha = 1.0 - beta;
    R = (int)(radius * (alpha *   0.0 + beta *   0.0));
    G = (int)(radius * (alpha * 255.0 + beta * 255.0));
    B = (int)(radius * (alpha * 255.0 + beta *   0.0));
  }
  // interpolation between green (0.5 * M_PI) and yellow (0.75 * M_PI)
  if ((phi >= 0.5 * M_PI) && (phi < 0.75 * M_PI)) {
    beta  = (phi - 0.5 * M_PI) / (0.25 * M_PI);
    alpha = 1.0 - beta;
    R = (int)(radius * (alpha * 0.0   + beta * 255.0));
    G = (int)(radius * (alpha * 255.0 + beta * 255.0));
    B = (int)(radius * (alpha * 0.0   + beta * 0.0));
  }
  // interpolation between yellow (0.75 * M_PI) and red (Pi)
  if ((phi >= 0.75 * M_PI) && (phi <= M_PI)) {
    beta  = (phi - 0.75 * M_PI) / (0.25 * M_PI);
    alpha = 1.0 - beta;
    R = (int)(radius * (alpha * 255.0 + beta * 255.0));
    G = (int)(radius * (alpha * 255.0 + beta *   0.0));
    B = (int)(radius * (alpha * 0.0   + beta *   0.0));
  }
  if (R<0) R=0;
  if (G<0) G=0;
  if (B<0) B=0;
  if (R>255) R=255;
  if (G>255) G=255;
  if (B>255) B=255;
}

// flowToImage
void flowToImage(const blitz::Array<blitz::TinyVector<int,2>,2 >& flow, blitz::Array< blitz::TinyVector<float,3>, 2>& image, float scaleFactor = 1.0) {
  image.resize( flow.shape());
  for (int row = 0; row < flow.extent(0); row++) {
      for (int col = 0; col < flow.extent(1); col++) {
	float R,G,B;
	cartesianToRGB(scaleFactor*flow(row,col)[1],scaleFactor*flow(row,col)[0],R,G,B);
	image(row,col)[0] = R;
	image(row,col)[1] = G;
	image(row,col)[2] = B;
      }
  }
}
