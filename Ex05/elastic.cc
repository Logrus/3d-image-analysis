// ===============================
//
// 3D IMAGE ANALYSIS
//
// Exercise 05. Dense elastic registration
// Vladislav Tananaev
// tananaev@cs.uni-freiburg.de
//
// ===============================

#define BZ_DEBUG
#include <iostream>
#include <string>
#include <stdio.h>
#include <blitz/array.h>
#include "BlitzHDF5Helper.hh"
#include "my_blitz.hh"
#include "Fast_PD/Fast_PD.h"
#include "FlowToImage.hh"
using namespace std;

#define CLIP(minv, val, maxv) (max(minv,min(val,maxv)))

void computeDenseControlPointGraph( int nRows, int nCols,
                                    blitz::Array< blitz::TinyVector<int, 2>, 1> &nodes,
                                    blitz::Array< blitz::TinyVector<int, 2>, 1> &edges){

  int i(0);
  int i_edges(0);

  for (int row=0; row < nRows; ++row)
  for (int col=0; col < nCols; ++col)
  {
    nodes(i) = blitz::TinyVector<int,2> (row, col);
    
    if (row > 0) {
      edges(i_edges) = blitz::TinyVector<int,2> (i, i-nCols);
      ++i_edges;
    }
    
    if (col > 0){
      edges(i_edges) = blitz::TinyVector<int,2> (i, i-1);
      ++i_edges;
    }

    ++i;
  }
}

void computeDenseDisplacementHypotheses(int radius, blitz::Array< blitz::TinyVector<int,2>, 1> &labels){

  if (radius <= 0 )
    cerr << "Error: The radius for hypothesis generation is less or equal to zero." << endl;
  
  int i(0);
  for (int y=-radius; y <= radius; ++y)
  for (int x=-radius; x <= radius; ++x){
    if ( (abs(x) == abs(y)) || // Diagonals
         (x==0 && y!=0) || (x!=0 && y==0) ) // Cross
    {
      labels(i) = blitz::TinyVector<int, 2> (y,x);
      ++i;
    }
  }
  
}

// unaryCosts has the shape nLabels x nNodes
void computeUnaryCostsSSD( const blitz::Array< blitz::TinyVector<int, 2>, 1> &nodes,
                          const blitz::Array< blitz::TinyVector<int, 2>, 1> &labels,
                          const blitz::Array<float, 2> &srcIm,
                          const blitz::Array<float, 2> &trgIm,
                          blitz::Array<float, 2> &unaryCosts){

  blitz::TinyVector<int, 2> mediator;

  for (int l=0; l < labels.extent(0); ++l){

    for (int nod=0; nod < nodes.extent(0); ++nod){
  
      mediator = nodes(nod) + labels(l);
      int y = CLIP (0, mediator(0), trgIm.extent(0)-1);
      int x = CLIP (0, mediator(1), trgIm.extent(1)-1);
      float val = srcIm( nodes(nod) ) - trgIm ( y, x );
      unaryCosts(l, nod) = val*val;
    }

  }


}

// pairwiseCosts has the shape nLabels x nLabels
void computePairwiseCostL2( const blitz::Array< blitz::TinyVector<int,2>, 1> &labels,
                            blitz::Array<float, 2> &pairwiseCosts ){

  blitz::TinyVector<int,2> mediator;

  for (int i=0; i < labels.extent(0); ++i)
  for (int j=0; j < labels.extent(0); ++j)
  {
    mediator = blitz::pow2(labels(i) - labels(j));
    pairwiseCosts(i,j) = sqrt(mediator(0) + mediator(1));
  }

}

void warpImage( blitz::Array<float, 2> &warpedBackTrgIm,
                const blitz::Array<float, 2> &trgIm,
                const blitz::Array<blitz::TinyVector<int,2>, 2> &deformationField){

  for (int row = 0; row < trgIm.extent(0); ++row){ 
      for (int col = 0; col < trgIm.extent(1); ++col){
        int yval = CLIP(0, deformationField(row, col)(0) + row, trgIm.extent(0)-1 );
        int xval = CLIP(0, deformationField(row, col)(1) + col, trgIm.extent(1)-1 );
        warpedBackTrgIm(row, col) = trgIm(yval, xval);
      }
    }

}

blitz::Array< float, 2> createGridImage (int nRows, int nCols, int step){

  blitz::Array< float, 2> result(nRows, nCols);
  for (int i=0; i < result.extent(0); ++i)
  for (int j=0; j < result.extent(1); ++j){
    result(i,j) = 255;
    // Make black stripes
    if (i % step && j % step)
      result(i,j) = 0;
  }
  return result;
}

void computeUnaryCostsNCC( const blitz::Array< blitz::TinyVector<int,2>, 1> &nodes,
                           const blitz::Array< blitz::TinyVector<int,2>, 1> &labels,
                           const blitz::Array<float,2> &srcIm,
                           const blitz::Array<float,2> &trgIm,
                           blitz::Array< float, 2> &unaryCosts ){

  int r = 3; // Patch radius
  int size = r*2+1; // Patch size
  int n = size*size; // Number of elements
  float max_intencity(0);
  blitz::TinyVector<int, 2> mediator;
  blitz::Array<float, 2> srcPatch(size, size);
  blitz::Array<float, 2> trgPatch(size, size);

  
  for (int l=0; l < labels.extent(0); ++l){ // For each label
    for (int nod=0; nod < nodes.extent(0); ++nod){ // For each pixel

      mediator = nodes(nod) + labels(l);

      if (srcIm ( nodes(nod) ) > max_intencity)
        max_intencity = srcIm ( nodes(nod) );

      // Get two patches
      for (int pr = -r; pr <= r; ++pr)
        for (int pc = -r; pc <= r; ++pc){

          // Check borders for target
          float trgVal(0);
          int row_p = mediator(0)+pr, col_p = mediator(1)+pc;
          if (row_p>=0 && col_p>=0 && row_p<trgIm.extent(0) && col_p<trgIm.extent(1) )
            trgVal = trgIm ( row_p, col_p );

          // Check borders for source
          float srcVal(0);
          int r_s_p = nodes(nod)(0)+pr, c_s_p = nodes(nod)(1)+pc;
          if ( r_s_p>=0 && c_s_p>=0 && r_s_p<srcIm.extent(0) && c_s_p<srcIm.extent(1) )
            srcVal = srcIm ( r_s_p, c_s_p );

          srcPatch(pr+r, pc+r) = srcVal;
          trgPatch(pr+r, pc+r) = trgVal;
      }

      // Compute mean
      float mu_src = blitz::sum(srcPatch)/n;
      float mu_trg = blitz::sum(trgPatch)/n;

      // Compute std 
      float val(0), sum_trg(0), sum_src(0); 
      for (int row = 0; row < size; ++row)
        for (int col = 0; col < size; ++col){
          float src_demeaned = (srcPatch(row,col) - mu_src);
          float trg_demeaned = (trgPatch(row,col) - mu_trg);
          sum_src = sum_src + src_demeaned*src_demeaned;
          sum_trg = sum_trg + trg_demeaned*trg_demeaned;
          val = val + (src_demeaned*trg_demeaned);
        }
        float sigma1 = sqrt( sum_src/n );
        float sigma2 = sqrt( sum_trg/n );

        unaryCosts(l, nod) = -max_intencity*val/(sigma1*sigma2 + 0.0025); // magic number - normalizer (helps if denominator is near 0)

    }
  }
}

blitz::Array<float, 2> equalizeSizes( int nRows, int nCols, const blitz::Array<float, 2> &image){
  
  int nRows_b = nRows > image.extent(0) ? nRows : image.extent(0);
  int nCols_b = nCols > image.extent(1) ? nCols : image.extent(1);
  blitz::Array<float, 2> result(nRows_b, nCols_b);
  result = 0;
  for (int row=0;row<image.extent(0);++row)
    for (int col=0;col<image.extent(1);++col){
      result(row, col) = image(row, col);
    }
    return result;
}
                      
int main()
{
  // Load images
  std::string srcImFilename="zebra2.h5";
  std::string trgImFilename="zebra1.h5";
  blitz::Array<float, 2> srcIm_t;
  //blitz::Array<float, 2> srcIm;
  blitz::Array<float, 2> trgIm;
  readHDF5toBlitz (srcImFilename, "image", srcIm_t);
  readHDF5toBlitz (trgImFilename, "image", trgIm);
  blitz::Array<float, 2> srcIm = equalizeSizes(trgIm.extent(0), trgIm.extent(1), srcIm_t);
  int nRows = trgIm.extent(0); //> srcIm.extent(0) ? trgIm.extent(0) : srcIm.extent(0);
  int nCols = trgIm.extent(1); // > srcIm.extent(1) ? trgIm.extent(1) : srcIm.extent(1);

  
  // Create nodes and edges
  int nNodes = nRows * nCols;
  int nEdges = (nRows-1)*nCols + (nCols-1)*nRows; // 4 neighboord
  blitz::Array < blitz::TinyVector<int, 2>, 1> nodes (nNodes);
  blitz::Array < blitz::TinyVector<int, 2>, 1> edges (nEdges);
  computeDenseControlPointGraph(nRows, nCols, nodes, edges);


  // Generate hypothesis
  int r = 40, nLabels = 8*r + 1; // 8 directions + 1 pixel in the center
  blitz::Array < blitz::TinyVector<int, 2>, 1> labels (nLabels);
  computeDenseDisplacementHypotheses(r, labels);
  

  // Compute unary cost
  blitz::Array< float, 2> unaryCosts(nLabels, nNodes);
  computeUnaryCostsSSD(nodes, labels, srcIm, trgIm, unaryCosts);
  

  // Compute and scale pairwise cost
  float lambda = 1000;
  blitz::Array< float, 2> pairwiseCosts(nLabels, nLabels);
  computePairwiseCostL2(labels, pairwiseCosts);
  pairwiseCosts = pairwiseCosts * lambda;


  // Optimization
  int max_iterations = 100;
  blitz::Array<float, 1> edgeWeights(nEdges);
  edgeWeights = 1.0;

  float* _unaryCosts = reinterpret_cast<float*>(unaryCosts.dataFirst());
  int*   _edges = reinterpret_cast<int*>(edges.dataFirst());
  float* _pairwiseCosts = reinterpret_cast<float*>(pairwiseCosts.dataFirst());
  float* _edgeWeights = reinterpret_cast<float*>(edgeWeights.dataFirst());

  CV_Fast_PD pd( nNodes, nLabels, _unaryCosts, nEdges, _edges, _pairwiseCosts, max_iterations, _edgeWeights );
  pd.run();

  blitz::Array<int, 1> optimalLabels(nNodes);

  for( int i = 0; i < nNodes; ++i) {
    optimalLabels(i)=pd._pinfo[i].label;
  }

  blitz::Array<blitz::TinyVector<int,2>,2 > deformationField(srcIm.shape());

  for(int i = 0;i < nNodes; ++i){
    deformationField(nodes(i)) = labels(optimalLabels(i));
  }

  // Warping image with deformation field
  blitz::Array<float, 2> warpedBackTrgIm(nRows, nCols);
  warpImage(warpedBackTrgIm, trgIm, deformationField);


  // Creating additional grid to view deformation
  blitz::Array<float, 2> grid = createGridImage(nRows, nCols, 5);
  blitz::Array<float, 2> warpedGrid(nRows, nCols);
  warpImage(warpedGrid, grid, deformationField);

  blitz::Array< blitz::TinyVector<float,3>, 2> image(nRows, nCols);
  flowToImage(deformationField, image);

  // Saving output
  std::string outFileName="out.h5";
  remove (outFileName.c_str()); // Otherwise we can re-write with some junk in place
  writeBlitzToHDF5 (srcIm, "srcIm", outFileName);
  writeBlitzToHDF5 (trgIm, "trgIm", outFileName);
  writeBlitzToHDF5 (warpedBackTrgIm, "transformed", outFileName);
  writeBlitzToHDF5 (grid, "grid", outFileName);
  writeBlitzToHDF5 (warpedGrid, "warpedGrid", outFileName);
  writeBlitzToHDF5 (image, "deformationField", outFileName);

  return 0;
}
